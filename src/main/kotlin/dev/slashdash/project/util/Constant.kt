package dev.slashdash.project.util
//Application constants
object Constant {

    // JWT Constants
    const val ACCESS_TOKEN_VALIDITY_SECONDS = (8 * 60 * 60).toLong()
    const val SIGNING_KEY = "fotima"
    const val TOKEN_PREFIX = "Bearer "
    const val HEADER_STRING = "Authorization"
    const val AUTHORITIES_KEY = "scopes"
    const val FULL_NAME = "full_name"


    // URLs
    const val API_PATH_PREFIX = "api/v1"
    const val WEB_PATH_PREFIX = "web"


    // ROLE Constants
    const val ROLE_ADMIN = "ADMIN"
    const val ROLE_USER = "USER"

    //Permissions
    const val PERMISSION_VIEW_PATIENT_LIST="VIEW_PATIENT_LIST"
    const val PERMISSION_VIEW_PATIENT_DATA="VIEW_PATIENT_DATA"

    const val PERMISSION_ADMIN="ADMIN"
    const val PERMISSION_ADMIN_DESC="Has access to admin panel of the system"
    const val PERMISSION_CREATE_ANNOUNCEMENT="CREATE_ANNOUNCEMENT"
    const val PERMISSION_CREATE_ANNOUNCEMENT_DESC="Users can post announcements on the news board"
    const val PERMISSION_CREATE_TASK="CREATE_TASK"
    const val PERMISSION_CREATE_TASK_DESC="Users can create and assign tasks to other users with lower role rank"
        const val PERMISSION_CALL_ALL_USERS="CALL_ALL_USERS"
    const val PERMISSION_CALL_ALL_USERS_DESC="Users can call all users"
    const val PERMISSION_CALL_ALL_USERS_LOWER_RANK="CALL_ALL_USERS_LOWER_RANK"
    const val PERMISSION_CALL_ALL_USERS_LOWER_RANK_DESC="Users can call all users with lower role rank"
    const val PERMISSION_MESSAGE_ALL_USERS="MESSAGE_ALL_USERS"
    const val PERMISSION_MESSAGE_ALL_USERS_DESC="Users can send messages to all users"
    const val PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK="MESSAGE_ALL_USERS_LOWER_RANK"
    const val PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK_DESC="Users can send messages to all users with lower role rank"

}