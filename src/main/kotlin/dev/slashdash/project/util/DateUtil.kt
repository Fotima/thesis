package dev.slashdash.project.util
//util for data manipulation with dates
import dev.slashdash.project.dto.UserScheduleDTO
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.model.enum.WorkingHourStatus
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


fun getWorkingTime(schedule: Schedule): UserScheduleDTO {
    var userSchedule: UserScheduleDTO=UserScheduleDTO()
    var current = LocalDateTime.now()
    var weekDay: DayOfWeek = current.dayOfWeek
    var start: LocalDateTime
    var end: LocalDateTime
    var hours: MutableList<Date> = mutableListOf()
    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");


    val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    var workingHourToday: WorkingHour = when (weekDay) {
        DayOfWeek.MONDAY -> schedule.mon!!
        DayOfWeek.TUESDAY -> schedule.tues!!
        DayOfWeek.WEDNESDAY -> schedule.wed!!
        DayOfWeek.THURSDAY -> schedule.thur!!
        DayOfWeek.FRIDAY -> schedule.fr!!
        DayOfWeek.SATURDAY -> schedule.sat!!
        DayOfWeek.SUNDAY -> schedule.sun!!
    }
    if (workingHourToday.isOffDay) {
        userSchedule.isOffDay = true
        return userSchedule
    }

    var startDateString: String = "${current.dayOfMonth.toString().padStart(2, '0')}" + "/" + "${current.monthValue.toString().padStart(2, '0')}" + "/" + "${current.year}"
    var endDateString: String = if (workingHourToday.endTimeIsNextDay ?: false) {
        "${current.dayOfMonth + 1}" + "/" + "${current.monthValue.toString().padStart(2, '0')}" + "/" + "${current.year}"
    } else {
        startDateString
    }
//    var dateString: String = "${current.dayOfMonth}" + "/" + "${current.monthValue.toString().padStart(2, '0')}" + "/" + "${current.year}"

    if (workingHourToday.startTime != null && workingHourToday.endTime != null && !workingHourToday.isOffDay) {
        var startString: String = timeFormatter.format(workingHourToday.startTime?.toLocalTime())
        var endString: String = timeFormatter.format(workingHourToday.endTime?.toLocalTime())
//        var string = "$dateString" + " " + "$startString"
        start = LocalDateTime.parse("$startDateString" + " " + "$startString", formatter)
        end = LocalDateTime.parse("$endDateString" + " " + "$endString", formatter)
        hours.add(Date.from(start.atZone(ZoneId.systemDefault()).toInstant()))
        hours.add(Date.from(end.atZone(ZoneId.systemDefault()).toInstant()))
    }
    userSchedule.workingHours=hours
    return userSchedule
}

fun getWorkingHour(schedule: Schedule?): String {
    if (schedule == null) {
        return "No Data"
    }
    val userScheduleDTO= getWorkingTime(schedule)

    if(userScheduleDTO.isOffDay){
        return "Day Off"
    }
   else if (userScheduleDTO.workingHours.isEmpty()) {
        return "No Data"
    } else {
        val formatter = SimpleDateFormat("HH:mm")
        return "${formatter.format(userScheduleDTO.workingHours[0])}" + " - " + "${formatter.format(userScheduleDTO.workingHours[1])}"
//        val now =Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant())
//        return if(workingHours[0].before(now) && workingHours[1].after(now)){
//            WorkingHourStatus.ISWORKINGHOUR
//        } else {
//            WorkingHourStatus.NOTWORKINGHOUR
//        }

    }


}

fun checkWorkingHours(schedule: Schedule?): WorkingHourStatus {
    if (schedule == null) {
        return WorkingHourStatus.NODATA
    }
    val userScheduleDTO= getWorkingTime(schedule)

    if(userScheduleDTO.isOffDay){
        return   WorkingHourStatus.NOTWORKINGHOUR
    }
 else   if (userScheduleDTO.workingHours.isEmpty()) {
        return WorkingHourStatus.NODATA
    } else {
        val now = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant())
        return if (userScheduleDTO.workingHours[0].before(now) && userScheduleDTO.workingHours[1].after(now)) {
            WorkingHourStatus.ISWORKINGHOUR
        } else {
            WorkingHourStatus.NOTWORKINGHOUR
        }

    }


}