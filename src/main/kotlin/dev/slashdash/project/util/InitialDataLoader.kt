package dev.slashdash.project.util

import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.PermissionRepo
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.util.Constant
import dev.slashdash.project.util.Constant.PERMISSION_ADMIN
import dev.slashdash.project.util.Constant.PERMISSION_ADMIN_DESC
import dev.slashdash.project.util.Constant.PERMISSION_CALL_ALL_USERS
import dev.slashdash.project.util.Constant.PERMISSION_CALL_ALL_USERS_DESC
import dev.slashdash.project.util.Constant.PERMISSION_CALL_ALL_USERS_LOWER_RANK
import dev.slashdash.project.util.Constant.PERMISSION_CALL_ALL_USERS_LOWER_RANK_DESC
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_ANNOUNCEMENT
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_ANNOUNCEMENT_DESC
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_TASK
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_TASK_DESC
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS_DESC
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK_DESC
import dev.slashdash.project.util.Constant.PERMISSION_VIEW_PATIENT_DATA
import dev.slashdash.project.util.Constant.PERMISSION_VIEW_PATIENT_LIST
import dev.slashdash.project.util.Constant.ROLE_ADMIN
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.stereotype.Component
import javax.transaction.Transactional

//Initial data loader
@Component
class InitialDataLoader : ApplicationListener<ContextRefreshedEvent> {

    internal var alreadySetup = true // Change to false on first run

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var roleRepo: RoleRepo


    @Autowired
    lateinit var permissionRepo: PermissionRepo

    @Transactional
    override fun onApplicationEvent(event: ContextRefreshedEvent) {

        if (alreadySetup)
            return
        val adminPermission: Permission = createPermissionIfNotFound(PERMISSION_ADMIN, PERMISSION_ADMIN_DESC)
        createPermissionIfNotFound(PERMISSION_CREATE_TASK, PERMISSION_CREATE_TASK_DESC)
        createPermissionIfNotFound(PERMISSION_CREATE_ANNOUNCEMENT, PERMISSION_CREATE_ANNOUNCEMENT_DESC)
        createPermissionIfNotFound(PERMISSION_CALL_ALL_USERS, PERMISSION_CALL_ALL_USERS_DESC)
        createPermissionIfNotFound(PERMISSION_CALL_ALL_USERS_LOWER_RANK, PERMISSION_CALL_ALL_USERS_LOWER_RANK_DESC)
        createPermissionIfNotFound(PERMISSION_MESSAGE_ALL_USERS, PERMISSION_MESSAGE_ALL_USERS_DESC)
        createPermissionIfNotFound(PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK, PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK_DESC)

        createRoleIfNotFound(ROLE_ADMIN, arrayListOf(adminPermission))
//        createRoleIfNotFound(ROLE_USER)

        val adminRole = roleRepo.findByName(ROLE_ADMIN)
        val user = User()
        user.firstname = "Admin"
        user.lastname = "Adminov"
        val encodedPass = BCrypt.hashpw("123", BCrypt.gensalt(4))
        user.password = encodedPass
        user.email = "admin@gmail.com"
        user.role = adminRole!!
        user.active = true

        userRepo.save(user)

        alreadySetup = true
    }


    @Transactional
    internal fun createRoleIfNotFound(name: String, permissions: MutableList<Permission>): Role {
        var role = roleRepo.findByName(name)
        if (role == null) {
            role = Role(name, permissions)
            role.rank = 0
            roleRepo.save(role)
        }
        return role
    }

    @Transactional
    internal fun createPermissionIfNotFound(name: String, description: String?): Permission {
        var permission = permissionRepo.findByName(name)
        if (permission == null) {
            permission = Permission(name, description)
            permissionRepo.save(permission)
        }
        return permission
    }
}