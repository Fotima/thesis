package dev.slashdash.project.util

import com.google.firebase.messaging.Notification
//util for Notification service

object NotificationUtils {

    val NOTIF_CODE_NEW_TASK = 1
    val NOTIF_CODE_NEW_MESSAGE = 2


    fun getNotificationFromCode( code: Int): Notification {
        return when(code) {
            NOTIF_CODE_NEW_TASK ->Notification("Hospital Name", "You have new task")
            NOTIF_CODE_NEW_MESSAGE ->Notification("Hospital Name", "You have new message")
            else -> Notification("Hospital Name", "You have notification")
        }
    }

}