package dev.slashdash.project.util

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.core.io.ClassPathResource

//util for Firebase service

class FirebaseUtils {
    companion object {
        fun initAdminSDK()
        {
//            val serviceAccount = FileInputStream(ResourceUtils.getFile("classpath:serviceAccountKey.json"))
            val serviceAccount = ClassPathResource("thesisadminfirebasekey.json").inputStream

            val options = FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://thesis-project-b4323.firebaseio.com")
                    .build()

            var hasBeenInitialized = false
            val firebaseApps = FirebaseApp.getApps()

            for (app in firebaseApps) {
                if (app.name == FirebaseApp.DEFAULT_APP_NAME) {
                    hasBeenInitialized = true
                }
            }

            if(!hasBeenInitialized) {
                FirebaseApp.initializeApp(options)
            }
        }
    }
}