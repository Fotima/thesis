package dev.slashdash.project.controller.rest

import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.model.Department
import dev.slashdash.project.service.deparment.IDepartmentService
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest Department controller

@RestController
@RequestMapping("/$API_PATH_PREFIX/department")
class DepartmentController {

    @Autowired
    lateinit var departmentService: IDepartmentService
//returns all departments from the server
    @GetMapping("/all")
    fun getAllPermissions(principal: Principal): List<Department> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return departmentService.getAllDepartments(principal)
    }
//creates a new department
//    if posted model contains id, existing department is updated
    @PostMapping()
    fun createUpdateDepartment(@RequestBody dto: Department, principal: Principal): Department {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return departmentService.createUpdateDepartment(dto,principal)
    }

//returns a department data by requested id
    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int, principal: Principal?): Department {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return departmentService.getDepartmentById(id,principal)
    }
//deleted a department by id
    @DeleteMapping("/{id}")
    fun deleteDepratment(@PathVariable id: Int, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return departmentService.deleteDepartment(id,principal);
    }

}