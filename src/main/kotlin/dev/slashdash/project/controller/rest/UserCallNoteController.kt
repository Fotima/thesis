package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.UserCallCreateDTO
import dev.slashdash.project.dto.UserCallNoteCreateDTO
import dev.slashdash.project.dto.UserCallNoteInfoDTO
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.service.user_call_note.UserCallNotService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest User Call noted controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/notes")
class UserCallNoteController {
    @Autowired
    lateinit var userCallNotService: UserCallNotService
//returns all call history of current user with notes
    @GetMapping("/my")
    fun getMyNotes(principal: Principal?): List<UserCallNoteInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userCallNotService.getMyCallNotes(principal)
    }
//    creates a new call history for a user

    @PostMapping("/create")
    fun createCallHistory(
            @RequestBody dto: UserCallCreateDTO,
            principal: Principal?): UserCallNoteInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userCallNotService.createCallHistory(principal, dto)
    }
//creates posted note for a call history
    @PostMapping("/addnote")
    fun addNoteToHistory(
            @RequestBody dto: UserCallNoteCreateDTO,
            principal: Principal?): UserCallNoteInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userCallNotService.addNoteToCall(principal, dto)
    }
}