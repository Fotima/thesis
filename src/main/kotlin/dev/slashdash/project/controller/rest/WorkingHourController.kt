package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.WorkingHourDTO
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.service.workingHour.WorkingHourService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest Working hour controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/workinghours")
class WorkingHourController{
    @Autowired
    lateinit var workingHourService: WorkingHourService

// returns pageable of working hours
    @GetMapping("/all")
    fun getAllWorkingHours(pageable: Pageable, principal: Principal?): List<WorkingHourDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return  workingHourService.getAllWorkingHours(principal)
    }
//creates or updates working hour data on the server
    @PostMapping()
    fun createUpdateWorkingHour(@RequestBody dto: WorkingHourDTO, principal: Principal?): WorkingHour {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return  workingHourService.createOrUpdateWorkingHour(dto,principal)
    }

//returns working hour data by requested id
    @GetMapping("/{id}")
    fun getById(@PathVariable id : Int, principal: Principal?): WorkingHourDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return workingHourService.getWorkingHourById(id,principal)
    }
//Delete working hour by requested id on the server
    @DeleteMapping("/{id}")
    fun deleteWorkingHour(@PathVariable id:Int, principal: Principal?){
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return workingHourService.deleteWorkingHour(id, principal);
    }
}