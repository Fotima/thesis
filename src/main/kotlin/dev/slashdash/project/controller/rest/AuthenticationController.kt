package dev.slashdash.project.controller.rest

import dev.slashdash.project.config.jwt.TokenProvider
import dev.slashdash.project.dto.SignInDTO
import dev.slashdash.project.dto.SignInResponseDTO
import dev.slashdash.project.exception.InvalidCredentialsException
import dev.slashdash.project.exception.UserNotFoundException
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*

//Rest Authetication controller

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
@RequestMapping("/$API_PATH_PREFIX")
class AuthenticationController {

    @Autowired
    private val authenticationManager: AuthenticationManager? = null

    @Autowired
    private val jwtTokenUtil: TokenProvider? = null

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var roleRepo: RoleRepo

//    Registers a new users
    @PostMapping(value = "/signup")
    @Throws(AuthenticationException::class)
    fun register(@RequestBody dto: SignInDTO): SignInResponseDTO {
        try {
            val authentication = authenticationManager!!.authenticate(
                    UsernamePasswordAuthenticationToken(
                            dto.username,
                            dto.password
                    )
            )
//            generate a new token
            SecurityContextHolder.getContext().authentication = authentication
            val token = jwtTokenUtil!!.generateToken(authentication)
            val user: User = userRepo.findByEmail(dto.username) ?: throw UserNotFoundException()
            val expireDate = Calendar.getInstance()
            expireDate.add(Calendar.DAY_OF_YEAR, 50)
            val emailConfirmed:Boolean = if(user.emailConfirmed == null)  false else user.emailConfirmed!!
            val hasLowerRoles=roleRepo.countAllByRankGreaterThan(user.role.rank!!)>0
//            return Sign in response model
            return SignInResponseDTO(token, expireDate.time, emailConfirmed, user, hasLowerRoles)
        } catch (e: Exception) {
            throw InvalidCredentialsException()
        }
    }

}