package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.model.Task
import dev.slashdash.project.service.task.TaskService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.awt.print.Pageable
import java.security.Principal
//Rest Task controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/task")
class TaskController {

    @Autowired
    lateinit var taskService: TaskService
//returns list of tasks from the server
    @GetMapping()
    fun getAllTasks(principal: Principal?): List<TaskInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.getAllTasks(principal)
    }
// returns list of tasks of current user for today
    @GetMapping("/me")
    fun getUserTasks(principal: Principal?): List<TaskInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.getUsersTasks(principal.name, principal)
    }
//creates a new task in the server
//    updates existing task if posted model contains id
    @PostMapping("/create")
    fun createUpdateTask(@RequestBody dto: TaskCreateDTO, principal: Principal?): TaskInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.createTask(dto, principal.name)
    }
//updates progress of the task
    @PostMapping("/status/update")
    fun updateTaskStatus(@RequestBody dto: TaskStatusUpdateDTO, principal: Principal?): TaskInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.updateTaskStatus(dto, principal)
    }
//returns list of tasks filtered by given contraints
    @PostMapping("/filter")
    fun filterTasks(@RequestBody dto: TaskFilterDTO, principal: Principal?): List<TaskInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.filterTasks(dto, principal)
    }
//    deleted the task by id

    @DeleteMapping("/{id}")
    fun deleteTask(@PathVariable id: Int, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return taskService.deleteTask(id, principal)
    }


}