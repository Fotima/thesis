package dev.slashdash.project.controller.rest

import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.repo.PermissionRepo
import dev.slashdash.project.service.permission.PermissionService
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest Permissions controller

@RestController
@RequestMapping("/$API_PATH_PREFIX/permission")
class PermissionController {
    @Autowired
    lateinit var permissionService: PermissionService

    @Autowired
    lateinit var permissionRepo: PermissionRepo
//returns list of permissions from the server
    @GetMapping("/all")
    fun getAllPermissions(pageable: Pageable, principal: Principal?): Page<Permission> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return permissionService.getAllPermissions(pageable,principal)
    }
//creates a new permission on the server
    @PostMapping("/create")
    fun createPermission(@RequestBody permission: Permission, principal: Principal?): Permission {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return permissionService.createPermission(permission,principal)
    }

    @PutMapping("/update")
    fun updatePermission(@RequestBody permission: Permission, principal: Principal?): Permission {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return permissionService.updatePermission(permission,principal)
    }
    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable id: Int, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return permissionService.deletePermission(id, principal)
    }
}