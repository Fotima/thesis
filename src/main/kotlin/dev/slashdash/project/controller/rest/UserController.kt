package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.GoogleUserNotFoundException
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import io.swagger.annotations.ApiOperation
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.servlet.http.HttpServletRequest

//Rest User controller

@RestController
@RequestMapping("/$API_PATH_PREFIX/user")
class UserController {
    @Autowired
    lateinit var userService: UserService

    private val logger: Logger = LogManager.getLogger(UserController::class.java)

    @GetMapping("/admin")
    fun admin(principal: Principal?): String {
        return "Success"
    }

    @GetMapping()
    fun user(principal: Principal?): String {
        return "Success User"
    }
//return user data by requested id
    @GetMapping("/{id}")
    fun getUserById(@PathVariable id: Int, principal: Principal?): UserInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getUserById(id, principal);
    }
//returns pageable of users from the server
    @GetMapping("/all")
    fun getAllUsers(pageable: Pageable, principal: Principal?): Page<UserInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getAllUsersWeb(pageable, principal)
    }
//returns list of users from the server
    @GetMapping("/list")
    fun getUsers(principal: Principal?): List<UserShortDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getAllUsers(principal)
    }
//creates a new user
//    if posted model includes id, updates the existing user model

    @PostMapping()
    fun createUpdateUser(userCreateDTO: UserCreateDTO, principal: Principal?): UserShortDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.createOrUpdateUser(userCreateDTO, principal)
    }
//delete a user by requested id
    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable id: Int, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.deleteUser(id, principal)
    }
// updates status of the user (check in, check out)
    @PostMapping("/status")
    fun updateUserStatus(@RequestBody userStatusDTO: UserStatusDTO, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.updateUserStatus(userStatusDTO, principal.name)
    }
//returns list of users with lower than current users' role rank
    @GetMapping("/byRank")
    fun getLowerRankUsers(principal: Principal?): List<UserShortDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getLowerRankUsers(principal.name)
    }
//confirms user account
    @PostMapping("/confirm")
    fun confirmUserEmail(
            @RequestBody dto: EmailConfirmDTO, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        userService.confirmUserEmail(dto.email, principal.name);
    }
//returns list of users grouped by a role
    @GetMapping("/contacts")
    fun getAllUserContacts(principal: Principal?): List<UsersByRoleDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getUserContacts(principal);
    }

    @GetMapping("/contacts/rank")
    fun getAllUserCaontactsLower(principal: Principal?): List<UsersByRoleDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getUserContactsRank(principal);
    }
//    returns profile data of current user

    @GetMapping("/mobile/profile")
    fun getMyProfileInfo(principal: Principal?): UserInfoMobileDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.getMyProfileData(principal);
    }
//updated the current user's profile data on the server
    @PostMapping("/mobile/update")
    fun updateMyProfileInfo(@RequestBody dto: UserDataUpdateMobileDTO, principal: Principal?): UserInfoMobileDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return userService.updateMyProfileData(principal, dto);
    }

}