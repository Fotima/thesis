package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.ChatMessageCreateDTO
import dev.slashdash.project.dto.ChatMessagePageableDTO
import dev.slashdash.project.dto.ChatRoomInfoDTO
import dev.slashdash.project.service.chat.ChatRoomService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid
//Rest Chat controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/chat")
class ChatController {
    @Autowired
    lateinit var chatRoomService: ChatRoomService
//creates a chat room with current user and give user
    @GetMapping("/create")
    fun initiateConversation(@RequestParam withUserId: Int, principal: Principal): ChatRoomInfoDTO {
        return chatRoomService.createNewChatRoom(withUserId, principal)
    }
//returns list of chat rooms where the current user is involved
    @GetMapping("/list")
    fun getAllChatsOfUser(principal: Principal): List<ChatRoomInfoDTO> {
        return chatRoomService.getAllChatsOfUser(principal)
    }

//returns pageable of messages from requested chat room
    @GetMapping("/messages")
    fun getAllChatMessages(principal: Principal, pageable: Pageable, @RequestParam chatId: Int?, @RequestParam withUserId: Int?): ChatMessagePageableDTO {
        return chatRoomService.getChatMessagesPaged(chatId, withUserId, principal, pageable)
    }
//creates a new chat message in a given chat room
    @PostMapping("/message/send")
    fun sendMessage(@Valid @RequestBody dto: ChatMessageCreateDTO, principal: Principal): MutableMap<String, Boolean> {
        return chatRoomService.createMessage(dto, principal)
    }

}