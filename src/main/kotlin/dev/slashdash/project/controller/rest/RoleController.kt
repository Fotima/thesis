package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.RoleCreateDTO
import dev.slashdash.project.dto.RoleInfoDTO
import dev.slashdash.project.dto.RoleInfoMobileDTO
import dev.slashdash.project.dto.RoleShortDTO
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.service.role.RoleService
import dev.slashdash.project.util.Constant
import org.apache.logging.log4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest Role controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/role")
class RoleController {
    @Autowired
    lateinit var roleService: RoleService
//get all roles registered on the server by pageable
    @GetMapping("/all")
    fun getAllRoles(pageable: Pageable, principal: Principal): Page<RoleInfoDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }

        return roleService.getAllRoles(pageable,principal)
    }


    @GetMapping("/mobile/all")
    fun getAllRolesMobile(pageable: Pageable, principal: Principal): Page<RoleInfoMobileDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return roleService.getAllRolesMobile(pageable,principal)
    }
//create a new role
//    if posted model contains id, existing role is updated
    @PostMapping()
    fun createUpdateRole(@RequestBody dto: RoleCreateDTO, principal: Principal): RoleInfoDTO {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return roleService.createOrUpdateRole(dto,principal)
    }

//get role by id
    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int, principal: Principal?): Role {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return roleService.getRoleById(id,principal)
    }
//delete role by requested id
    @DeleteMapping("/{id}")
    fun deleteRole(@PathVariable id: Int, principal: Principal?) {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return roleService.deleteRole(id,principal);
    }
//get list of roles with lower or equal rank of current user
    @GetMapping("/lowerRank")
    fun getAllRolesOfLowerRank(principal: Principal?): List<RoleShortDTO> {
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return roleService.getAllRolesOfLowerRank(principal.name,principal)
    }
}