package dev.slashdash.project.controller.rest

import dev.slashdash.project.dto.AnnouncementCreateDTO
import dev.slashdash.project.dto.AnnouncementInfoDTO
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.service.announcement.AnnouncementService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.security.Principal
//Rest Announcement controller
@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/announcement")
class AnnouncementController {
    @Autowired
    lateinit var announcementService: AnnouncementService
//returns all announcements in the server
    @GetMapping("/list")
    fun getAll(principal: Principal?):List<AnnouncementInfoDTO>{
//    return exception if the header doesn't contain token
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return announcementService.getAllAnnouncements()
    }
//    creates a new announcement in the server
    @PostMapping("/create")
    fun createNew(@RequestBody dto:AnnouncementCreateDTO, principal: Principal?):AnnouncementInfoDTO{
        if (principal == null) {
            throw InvalidJWTTokenException()
        }
        return announcementService.createAnnouncement(dto, principal)
    }

}