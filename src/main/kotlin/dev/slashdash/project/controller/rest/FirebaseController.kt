package dev.slashdash.project.controller.rest

import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.GoogleUserNotFoundException
import dev.slashdash.project.exception.InvalidJWTTokenException
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.servlet.http.HttpServletRequest

//Rest Firebase controller

@RestController
@RequestMapping("/${Constant.API_PATH_PREFIX}/firebase")
@Api(description = "Actions for Firebase API")
class FirebaseController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userRepo: UserRepo
//saves a firebase token of a user on the server
    @PostMapping("/save/{token}" )
    @ApiOperation("Save ΩFirebase Token")
    fun confirmUserRegistration(
            @PathVariable token: String, principal: Principal?,
            request: HttpServletRequest)
            : MutableMap<String, Boolean> {
        if(principal==null){
            throw InvalidJWTTokenException()

        }
        val savedSuccessfully = userService.saveFirebaseToken(principal.name, token)
        val result: MutableMap<String, Boolean> = mutableMapOf()
        result["result"] = savedSuccessfully
        return result
    }
}