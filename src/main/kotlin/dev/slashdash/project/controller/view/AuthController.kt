package dev.slashdash.project.controller.view

import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import java.security.Principal
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
// Announcement controller

@Controller
class AuthController {
//navigates to login page
    @GetMapping("/login")
    fun getLogin(model: Model): String {
        return "login"
    }
//navigates to login pages and removes the security data
    @GetMapping("/logout")
    fun logoutPage(request: HttpServletRequest, response: HttpServletResponse): String {
        val auth = SecurityContextHolder.getContext().authentication
        if (auth != null) {
            SecurityContextLogoutHandler().logout(request, response, auth)
        }
        return "redirect:/login?logout"
    }
}