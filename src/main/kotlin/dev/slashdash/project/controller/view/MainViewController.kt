package dev.slashdash.project.controller.view//package dev.slashdash.project.controller.view

import dev.slashdash.project.repo.DepartmentRepo
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView
import java.security.Principal
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
//main view controller
@Controller
class MainViewController {
    @Autowired
    lateinit var  userService: UserService

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var departmentRepo: DepartmentRepo

    @Autowired
    lateinit var roleRepo: RoleRepo

    @GetMapping("/")
    fun redirectToIndex(): ModelAndView {

        return ModelAndView("redirect:/$WEB_PATH_PREFIX/main")
    }
//    gets statistical data and navigates to dashboard web page
    @GetMapping("/$WEB_PATH_PREFIX/main")
    fun getDashboard(model: Model, principal: Principal): String {

        val activeUsers= userService.getAllCheckinUsers(principal)
        val usersCount=userRepo.countAllByActiveTrue()
        val activeCount =userRepo.countAllByCheckedInTrue()
        val departmentCount=departmentRepo.count()
        val roleCount=roleRepo.count()
        model.addAttribute("activeUsers", activeUsers)
        model.addAttribute("usersCount", usersCount)
        model.addAttribute("activeCount", activeCount)
        model.addAttribute("departmentCount", departmentCount)
        model.addAttribute("roleCount", roleCount)

        return "admin"
    }

}