package dev.slashdash.project.controller.view

import dev.slashdash.project.dto.UserInfoDTO
import dev.slashdash.project.model.PageWrapper
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.service.userLogs.UserLogsService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal
//User logs controller

@Controller
@RequestMapping("/${Constant.WEB_PATH_PREFIX}/userLogs")
class UserLogsViewController {

    @Autowired
    lateinit var userLogsService: UserLogsService
//    returns pageable of logs for the latest week
    @GetMapping
    fun getuserLogsForWeek(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }

        var logs: Page<UserLogs> = userLogsService.getAllLogsForLastWeek(pageable)
        val wrapper = PageWrapper(logs, "/${Constant.WEB_PATH_PREFIX}/userLogs")
        model.addAttribute("logs", wrapper.getContent())
        model.addAttribute("page", wrapper)
        return "user_logs/logs_list"
    }
}
