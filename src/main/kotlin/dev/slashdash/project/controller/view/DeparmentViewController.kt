package dev.slashdash.project.controller.view

import dev.slashdash.project.dto.DeleteForm
import dev.slashdash.project.model.Department
import dev.slashdash.project.service.deparment.IDepartmentService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import springfox.documentation.annotations.ApiIgnore
import java.security.Principal
import javax.validation.Valid
// Department controller

@Controller
@ApiIgnore
@RequestMapping("/${Constant.WEB_PATH_PREFIX}/departments")
class DeparmentViewController {

    @Autowired
    lateinit var departmentService: IDepartmentService

//get list of all department from the server and navigates to departments list web page
    @GetMapping("/all")
    fun getAllDepartments(model: Model, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        var deparments: List<Department> = departmentService.getAllDepartments(principal)
        model.addAttribute("departments", deparments)
        return "department/department_list"
    }
//    if the id data is supplied, the department data by id is received and navigated to department create/update
//    web page, otherwise the function just navigates to  department create/update web page

    @GetMapping("/{id}")
    fun getCreateAndUpdatePage(@PathVariable id: Int, model: Model,principal: Principal): String {
        if (id > 0) {
            val department = departmentService.getDepartmentById(id,principal)

            model.addAttribute("item", department)
        } else {
            model.addAttribute("item", Department())
        }

        return "department/create_update"


    }

//updates existing department on the server and redirects user to home web page
    @PostMapping("/update")
    fun updateDepartment(model: Model,
                   @ModelAttribute @Valid form: Department,
                   bindingResult: BindingResult,principal: Principal): ModelAndView {

        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments" + form.id)

        } else {
            departmentService.createUpdateDepartment(form,principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments")
        }

    }
//creates a new department on the server and redirects user to home web page
    @PostMapping("/create")
    fun createRole(model: Model,
                   @ModelAttribute @Valid form: Department,
                   bindingResult: BindingResult,principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments/" + 0)

        } else {
            departmentService.createUpdateDepartment(form,principal)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments/all")

        }

    }
//deletes the requested department and redirects user to home web page
    @PostMapping("/delete")
    fun delete(model: Model,
               @ModelAttribute @Valid form: DeleteForm,
               bindingResult: BindingResult,principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments")
        } else {
            departmentService.deleteDepartment(form.id,principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/departments")
        }

    }
}