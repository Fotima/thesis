package dev.slashdash.project.controller.view


import dev.slashdash.project.model.PageWrapper
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.service.permission.PermissionService
import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.security.Principal

//Permission View controller
@Controller
@RequestMapping("/${WEB_PATH_PREFIX}/permissions")
class PermissionViewController {
    @Autowired
    lateinit var permissionService: PermissionService


//gets pageable of departments from the server and navigates to list of permission web page
    @GetMapping()
    fun get(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        var permissions: Page<Permission> = permissionService.getAllPermissions(pageable,principal)
        val wrapper = PageWrapper(permissions, "/$WEB_PATH_PREFIX/permissions")
        model.addAttribute("permissions", wrapper.getContent())
        model.addAttribute("page", wrapper)
        return "permissions/permissions_list"
    }




}
