package dev.slashdash.project.controller.view

import dev.slashdash.project.dto.*
import dev.slashdash.project.model.PageWrapper
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.repo.PermissionRepo
import dev.slashdash.project.service.role.RoleService
import dev.slashdash.project.util.Constant
import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import java.security.Principal
import javax.validation.Valid

//Role View controller
@Controller
@RequestMapping("/${WEB_PATH_PREFIX}/roles")
class RoleViewController {
    @Autowired
    lateinit var roleService: RoleService


    @Autowired
    lateinit var permissionRepo: PermissionRepo

//    get pageable of roles from the server and navigate to the list of roles web page
    @GetMapping()
    fun getAllRoles(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        var roles: Page<RoleInfoDTO> = roleService.getAllRoles(pageable,principal)
        val wrapper = PageWrapper(roles, "/$WEB_PATH_PREFIX/roles")
        model.addAttribute("roles", wrapper.getContent())
        model.addAttribute("page", wrapper)
        return "roles/roles_list"
    }

//    if the id data is supplied, the role data by id is received and navigated to role create/update
//    web page, otherwise the function just navigates to  role create/update web page

    @GetMapping("/{id}")
    fun getCreateAndUpdatePage(@PathVariable id: Int, model: Model, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        if (id > 0) {
            val role = roleService.getRoleById(id, principal)

            model.addAttribute("item", role)
        } else {
            model.addAttribute("item", Role())
        }
        var permissions = permissionRepo.findAll()
        model.addAttribute("permissions", permissions)
        return "roles/create_update"


    }

//udpates existing role
    @PostMapping("/update")
    fun updateRole(model: Model,
                       @ModelAttribute @Valid form: RoleCreateDTO,
                       bindingResult: BindingResult, principal: Principal): ModelAndView {

        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            ModelAndView("redirect:/$WEB_PATH_PREFIX/roles")

        } else {
            roleService.createOrUpdateRole(form,principal)
            ModelAndView("redirect:/$WEB_PATH_PREFIX/roles")
        }

    }
//creates a new role and redirects to list of roles web page
    @PostMapping("/create")
    fun createRole(model: Model,
                       @ModelAttribute @Valid form: RoleCreateDTO,
                       bindingResult: BindingResult, principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/roles")

        } else {
            roleService.createOrUpdateRole(form,principal)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/roles")

        }

    }
//deletes existing role from the server
    @PostMapping("/delete")
    fun delete(model: Model,
               @ModelAttribute @Valid form: DeleteForm,
               bindingResult: BindingResult, principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/roles")
        } else {
            roleService.deleteRole(form.id,principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/roles")
        }

    }
}
