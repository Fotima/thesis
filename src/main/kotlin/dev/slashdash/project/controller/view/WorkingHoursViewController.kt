package dev.slashdash.project.controller.view

import dev.slashdash.project.dto.DeleteForm
import dev.slashdash.project.dto.WorkingHourDTO
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.service.workingHour.WorkingHourService
import dev.slashdash.project.util.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import java.security.Principal
import java.sql.Time
import javax.validation.Valid
//Working horus controller
@Controller
@RequestMapping("/${Constant.WEB_PATH_PREFIX}/workinghours")
class WorkingHoursViewController{
    @Autowired
    lateinit var workingHourService: WorkingHourService

//gets all working hours from the server and navigates to working hours list pages
    @GetMapping()
    fun getAll(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        var workingHours: List<WorkingHourDTO> = workingHourService.getAllWorkingHours(principal)
        model.addAttribute("workingHours", workingHours)
        return "working_hours/working_hours_list"
    }
//    if the id data is supplied, the working hours data by id is received and navigated to working hours create/update
//    web page, otherwise the function just navigates to working hours create/update web page

    @GetMapping("/{id}")
    fun getCreateAndUpdatePage(@PathVariable id: Int, model: Model, principal: Principal): String {
        if (id > 0) {
            val workingHour = workingHourService.getWorkingHourById(id,principal)

            model.addAttribute("item", workingHour)
        } else {
            model.addAttribute("item", WorkingHour())
        }
        return "working_hours/create_update"


    }

//udpate working hour on the server
    @PostMapping("/update")
    fun updateWorkingHour(model: Model,
                   @ModelAttribute @Valid form: WorkingHourDTO,
                   bindingResult: BindingResult, principal: Principal): ModelAndView {

        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")

        } else {
            workingHourService.createOrUpdateWorkingHour(form, principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")
        }

    }
//create working hour on the server

    @PostMapping("/create")
    fun createWorkingHour(model: Model,
                   @ModelAttribute @Valid form: WorkingHourDTO,
                   bindingResult: BindingResult, principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)

            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")

        } else {
            workingHourService.createOrUpdateWorkingHour(form, principal)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")

        }

    }
//delete existing working hour from the server

    @PostMapping("/delete")
    fun delete(model: Model,
               @ModelAttribute @Valid form: DeleteForm,
               bindingResult: BindingResult, principal: Principal): ModelAndView {
        return if (bindingResult.hasErrors()) {
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")
        } else {
            workingHourService.deleteWorkingHour(form.id, principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/workinghours")
        }

    }
}