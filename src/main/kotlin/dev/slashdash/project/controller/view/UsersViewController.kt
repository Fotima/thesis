package dev.slashdash.project.controller.view

import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.EntityNotCreatedException
import dev.slashdash.project.model.PageWrapper
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.DepartmentRepo
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.WorkingHourRepo
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant
import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import java.security.Principal
import javax.validation.Valid
//User  controller

@Controller
@RequestMapping("/${WEB_PATH_PREFIX}/users")
class UsersViewController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var roleRepo: RoleRepo

    @Autowired
    lateinit var workingHourRepo: WorkingHourRepo

    @Autowired
    lateinit var departmentRepo: DepartmentRepo

//gets pageable of users and navigates a user to user list web page
    @GetMapping()
    fun getAllUsers(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }

        var users: Page<UserInfoDTO> = userService.getAllUsersWeb(pageable,principal)
        val wrapper = PageWrapper(users, "/$WEB_PATH_PREFIX/users")
        model.addAttribute("users", wrapper.getContent())
        model.addAttribute("page", wrapper)
        return "users/users_list"
    }
//    if the id data is supplied, the user data by id is received and navigated to user create/update
//    web page, otherwise the function just navigates to  user create/update web page

    @GetMapping("/{id}")
    fun getCreateAndUpdatePage(@PathVariable id: Int, model: Model, principal: Principal): String {
        if (id > 0) {
            val user = userService.getUser(id,principal)

            model.addAttribute("item", user)
        } else {
            model.addAttribute("item", User())
        }
        var roles = roleRepo.findAll()
        var departments = departmentRepo.findAll()
        model.addAttribute("roles", roles)
        model.addAttribute("departments", departments)
        return "users/create_update"


    }

//updates existing role on the server
    @PostMapping("/update")
    fun updateUser(model: Model,
                   @ModelAttribute @Valid form: UserCreateDTO,
                   bindingResult: BindingResult, principal: Principal): ModelAndView {

        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            ModelAndView("redirect:/$WEB_PATH_PREFIX/users/" + form.id)

        } else {
            if ((!userService.checkIfEmployeeIdIsUnique(form.employeeId, form.id))) {
                throw EntityNotCreatedException("Employee id already exists")
            }

            if ((!userService.checkIfEmailIsUnique(form.email, form.id))) {
                throw EntityNotCreatedException("Email already exists")
            }

            userService.createOrUpdateUser(form, principal)
            ModelAndView("redirect:/$WEB_PATH_PREFIX/users")
        }

    }
//creates a new user on the server
    @PostMapping("/create")
    fun createUser(model: Model,
                   @ModelAttribute @Valid form: UserCreateDTO,
                   bindingResult: BindingResult, principal: Principal): ModelAndView {
        System.out.println("create " + form)
        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/users/" + 0)

        } else {
            if (!(userService.checkIfEmployeeIdIsUnique(form.employeeId, form.id))) {
                throw EntityNotCreatedException("Employee id already exists")
            }

            if (!(userService.checkIfEmailIsUnique(form.email, form.id))) {
                throw EntityNotCreatedException("Email already exists")
            }


            userService.createOrUpdateUser(form, principal)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/users")

        }

    }
//deletes existing user from the server
    @PostMapping("/delete")
    fun delete(model: Model,
               @ModelAttribute @Valid form: DeleteForm,
               bindingResult: BindingResult, principal: Principal): ModelAndView {
        System.out.println("Delete ${form.id}")
        return if (bindingResult.hasErrors()) {
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}")
        } else {
            userService.deleteUser(form.id, principal)
            ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/users")
        }

    }
//gets a schedule of a user from the server and navigates to schedule web page
    @GetMapping("/schedule/{id}")
    fun getUserChedule(@PathVariable id: Int, model: Model, principal: Principal): String {
        val user: User = userService.getUser(id, principal)
        model.addAttribute("user", user)
        var schedule: Schedule
        if (user.schedule == null) {
            schedule = Schedule()
        } else {
            schedule = user.schedule!!
        }

        model.addAttribute("item", schedule)
        val workingHours = workingHourRepo.findAll()
        model.addAttribute("workingHours", workingHours)
        return "schedule/create_update"
    }

//    updates schedule of user on the server
    @PostMapping("/schedule/update/{id}")
    fun updateUserSchedule(model: Model,
                           @PathVariable id: Int,
                           @ModelAttribute @Valid form: ScheduleCreateDTO,
                           bindingResult: BindingResult, principal: Principal): ModelAndView {

        return if (bindingResult.hasErrors()) {
            model.addAttribute("item", form)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/users")

        } else {
            userService.updateUserSchedule(form, id,principal)
            return ModelAndView("redirect:/${Constant.WEB_PATH_PREFIX}/users")

        }

    }
//returns pageable of all users and their schedule and navigates to user schedule list web page
    @GetMapping("/schedule/all")
    fun getAllUsersWithSchedule(model: Model, pageable: Pageable, principal: Principal): String {
        if (principal == null) {
            return "redirect:/login"
        }
        var users: Page<UserScheduleInfoDTO> = userService.getAllUsersSchedule(pageable, principal)
        val wrapper = PageWrapper(users, "/$WEB_PATH_PREFIX/users/schedule/all")
        model.addAttribute("users", wrapper.getContent())
        model.addAttribute("page", wrapper)
        return "schedule/users_schedule_list"
    }


}
