package dev.slashdash.project

import dev.slashdash.project.controller.view.UsersViewController
import dev.slashdash.project.util.FirebaseUtils
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class ProjectApplication


fun main(args: Array<String>) {
	FirebaseUtils.initAdminSDK()

	runApplication<ProjectApplication>(*args)
}




