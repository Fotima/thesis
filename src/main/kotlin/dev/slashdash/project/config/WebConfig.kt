package dev.slashdash.project.config

import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.config.annotation.*
import org.springframework.web.servlet.i18n.CookieLocaleResolver
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import java.util.*

//Web MVC configuration
@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

    @Bean(name = ["localeResolver"])
    fun getLocaleResolver(): LocaleResolver {
        val resolver = CookieLocaleResolver()
        resolver.cookieDomain = "myAppLocaleCookie"
        resolver.cookieMaxAge = 60 * 60
        return resolver
    }

    /*
     * Create LocaleResolver Bean
     */
    @Bean
    fun localeResolver(): LocaleResolver {
        val resolver = CookieLocaleResolver()
        resolver.setDefaultLocale(Locale("en")) // your default locale
        return resolver
    }

    @Bean(name = ["messageSource"])
    fun getMessageResource(): MessageSource {
        val messageResource = ReloadableResourceBundleMessageSource()

        messageResource.setBasename("classpath:i18n/messages")
        messageResource.setDefaultEncoding("UTF-8")
        return messageResource
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        val localeInterceptor = LocaleChangeInterceptor()
        localeInterceptor.paramName = "lang"

        registry.addInterceptor(localeInterceptor).addPathPatterns("/*")
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry
                .addResourceHandler(
                        "/img/**",
                        "/css/**",
                        "/fonts/**",
                        "/js/**")
                .addResourceLocations(
                        "classpath:/static/fonts/",
                        "classpath:/static/img/",
                        "classpath:/static/css/",
                        "classpath:/static/js/")
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}