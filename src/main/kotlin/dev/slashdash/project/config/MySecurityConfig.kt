package dev.slashdash.project.config

import dev.slashdash.project.config.jwt.JwtAuthenticationEntryPoint
import dev.slashdash.project.config.jwt.JwtAuthenticationFilter
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_ANNOUNCEMENT
import dev.slashdash.project.util.Constant.PERMISSION_CREATE_TASK
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS
import dev.slashdash.project.util.Constant.PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK
import dev.slashdash.project.util.Constant.PERMISSION_VIEW_PATIENT_LIST
import dev.slashdash.project.util.Constant.ROLE_ADMIN
import dev.slashdash.project.util.Constant.ROLE_USER
import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import java.util.*
import javax.annotation.Resource

//Security configuration and policies of an application
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class MySecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private val unauthorizedHandler: JwtAuthenticationEntryPoint? = null

    @Resource(name = "userService")
    private val userDetailsService: UserDetailsService? = null

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
//                .csrf().disable()
                .cors() // enable cross-origin support for all requests
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/favicon.ico", "/css/**", "/js/**", "/img/**", "/fonts/**", "/sass/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                //MVC
                .antMatchers("/$WEB_PATH_PREFIX").permitAll()
                .antMatchers("/$API_PATH_PREFIX/signup").permitAll()

                .antMatchers("/$API_PATH_PREFIX/announcement/create").hasAnyAuthority(PERMISSION_CREATE_ANNOUNCEMENT)
                .antMatchers("/$API_PATH_PREFIX/announcement/list").authenticated()
                .antMatchers("/$API_PATH_PREFIX/chat/create").hasAnyAuthority(PERMISSION_MESSAGE_ALL_USERS,PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK)
//                .antMatchers("/$API_PATH_PREFIX/chat/create").hasAnyAuthority(PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK)
                .antMatchers("/$API_PATH_PREFIX/chat/message/send").hasAnyAuthority(PERMISSION_MESSAGE_ALL_USERS,PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK)
//                .antMatchers("/$API_PATH_PREFIX/chat/message/send").hasAnyAuthority(PERMISSION_MESSAGE_ALL_USERS_LOWER_RANK)
                .antMatchers("/$API_PATH_PREFIX/task/create").hasAnyAuthority(PERMISSION_CREATE_TASK)
                .antMatchers("/$API_PATH_PREFIX/**").authenticated()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/web/main")
                .loginPage("/login").permitAll()
                .and()
                .logout()
                .logoutUrl("/logout") //the URL on which the clients should post if they want to logout
                .logoutSuccessUrl("/login?logout")!!.permitAll()
                .invalidateHttpSession(true)
                .permitAll().and().exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                .and()
                .httpBasic()

        http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter::class.java)
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = Arrays.asList("*")
        configuration.allowedMethods = Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")
        configuration.allowedHeaders = Arrays.asList("authorization", "content-type", "x-auth-token", "origin", "x-requested-with", "cache-control")
        configuration.allowCredentials = true
        configuration.exposedHeaders = Arrays.asList("x-auth-token")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    @Bean
    @Throws(Exception::class)
    fun authenticationTokenFilterBean(): JwtAuthenticationFilter {
        return JwtAuthenticationFilter()
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }


    @Autowired
    @Throws(Exception::class)
    fun globalUserDetails(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService<UserDetailsService>(userDetailsService)
                .passwordEncoder(encoder())
    }

    @Bean
    fun encoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }


}
