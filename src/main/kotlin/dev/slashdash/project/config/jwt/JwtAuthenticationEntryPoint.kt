package dev.slashdash.project.config.jwt

import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.security.core.AuthenticationException
import javax.servlet.http.HttpServletResponse
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import java.io.Serializable
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component

//security entry point, redirects the request to login page if the header doesn't contain api path prefix of api/v1
@Component
class JwtAuthenticationEntryPoint : AuthenticationEntryPoint, Serializable {

    @Throws(IOException::class)
    override fun commence(request: HttpServletRequest,
                          response: HttpServletResponse,
                          authException: AuthenticationException) {
        if(request.requestURI.contains(API_PATH_PREFIX))
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized")
        else
            response.sendRedirect("/login")
    }
}