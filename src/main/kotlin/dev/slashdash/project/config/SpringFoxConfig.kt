package dev.slashdash.project.config

import com.google.common.collect.Lists
import com.google.common.collect.Lists.newArrayList
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration
import springfox.documentation.PathProvider
import springfox.documentation.builders.ParameterBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.schema.ModelRef
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import springfox.documentation.spring.web.paths.RelativePathProvider
import java.util.*
import springfox.documentation.spi.service.contexts.SecurityContext


@Configuration
@EnableSwagger2
@Import(value = [BeanValidatorPluginsConfiguration::class])
class SpringFoxConfig {
    @Bean
    fun apiDocket(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("dev.slashdash.project"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Lists.newArrayList(securityContext()))
                .securitySchemes(Lists.newArrayList(apiKey()))
                .apiInfo(getApiInfo())
    }


    private fun apiKey(): ApiKey {
        return ApiKey("JWT", "Authorization", "header")
    }

    private fun securityContext(): SecurityContext {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/*.*"))
//                .forPaths(PathSelectors.regex("$API_PATH_PREFIX"))
                .build()
    }

    fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope
        return Lists.newArrayList(
                SecurityReference("JWT", authorizationScopes))
    }

}

@Bean
fun apiDocket(): Docket {
    return Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(getApiInfo())
}


private fun getApiInfo(): ApiInfo {
    return ApiInfo(
            "Duduk's API Documentation",
            "Here you can find most up-to-date documentation of Duduk's API",
            "0.1",
            "https://slashdash.dev",
            Contact("Khalilullo Abdumuminov", "http://kmdev.uz", "kmdev202@gmail.com"),
            "License",
            "https://slashdash.dev",
            Collections.emptyList()
    )
}