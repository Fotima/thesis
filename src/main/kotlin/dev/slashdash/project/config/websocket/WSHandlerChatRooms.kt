package dev.slashdash.project.config.websocket

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.gson.Gson
import dev.slashdash.project.config.jwt.TokenProvider
import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.CustomBadRequestException
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.LoggedError
import dev.slashdash.project.service.chat.ChatRoomService
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.stereotype.Component
import org.springframework.web.socket.*
import org.springframework.web.socket.handler.TextWebSocketHandler
import org.w3c.dom.Text
import java.lang.Exception
import java.util.concurrent.ConcurrentHashMap
//WS for a chat room
@Component
class WSHandlerChatRooms(
        val chatRoomService: ChatRoomService?,
        val jwtTokenUtil: TokenProvider?,
        val userService: UserService?,
        val authenticationManager: AuthenticationManager?
) : TextWebSocketHandler() {
    val gson = Gson()

    companion object {
        var newSession = ConcurrentHashMap<String, MutableList<SessionUserName>>()
    }

//removes the session from list of sessions when the connection is closed
    @Throws(Exception::class)
    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        newSession.forEach { _: String, value: MutableList<SessionUserName> -> value.removeIf { it.session == session } }
        System.out.println("Connection closed")
    }
//when the connection is established with a user, a new session with chat room id and token is created
    @Throws(Exception::class)
    override fun afterConnectionEstablished(session: WebSocketSession) {
        if (session.uri == null)
            return
        val query = session.uri!!.rawQuery ?: return
        val queryParams = query.split('?')
        if (queryParams.isEmpty()) {
            return
        }
        val token = queryParams.find { it.contains("token=") }.toString().substringAfter("token=").substringBefore("&")
        val chatRoomId = queryParams.find { it.contains("chat_id=") }.toString().substringAfter("chat_id=").substringBefore("&")

        if (token.isEmpty() && chatRoomId.isEmpty())
            return
        val userId = getUsernameFromToken(token)

        val handShakeHeaders = WebSocketHttpHeaders()
        handShakeHeaders.add("principal", userId)
        val sessionUserName = SessionUserName(session, userId)
        if (newSession[chatRoomId] == null)
            newSession[chatRoomId] = mutableListOf(sessionUserName)
        else
            newSession[chatRoomId]!!.add(sessionUserName)
        println("Chat Room WS: Size - ${newSession.size}; User ID: $userId (Chat Room ID: $chatRoomId)")
        super.afterConnectionEstablished(session)

    }
//    when a new message is received on ws service, it is sorted and sent to the right session and a new message is created

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        val messageType = gson.fromJson(message.payload, WSMessage::class.java)
//get required data from the message, such as chat room id, message content
        val chatRoomId = ((messageType.data as Map<*, *>)["chatId"] as Double).toInt()
        val text = ((messageType.data as Map<*, *>)["message"]).toString()
//        a new chat message object is created
        val dto = ChatMessageCreateDTO(chatRoomId, text)
        var sessionUsername: SessionUserName? = null
        val allSessions = newSession[chatRoomId.toString()]
        if (allSessions == null) {
            CustomBadRequestException("New message: Current user sessions is empty [${message.payload}]")
            return
        }

        val currentUsersSession = allSessions.filter { it.session == session && it.session!!.isOpen }

        if (currentUsersSession.isEmpty()) {
            CustomBadRequestException("New message: Current user sessions is empty [${message.payload}]")
            return
        }
        sessionUsername = currentUsersSession.first()

        val userId = sessionUsername.account
                ?: throw LoggedError(EntryNotFoundException("Can't find user #${sessionUsername.account} id in principal"))
        chatRoomService?.createMessageFromWS(dto, userId)
        println("WS: type: NEW_CHAT_MESSAGE incoming message session found: $message")

    }
fun broadcast(sessions: List<WebSocketSession>, msg: WSMessageDTO) {
        sessions.forEach { emit(it, msg) }
    }
// new message is broadcasted to all users connected to current session

    fun broadcastToSpecificUsers(sessions: List<WebSocketSession>, msg: WSMessageDTO) {
        sessions.forEach { emit(it, msg) }
    }

    fun emit(session: WebSocketSession, msg: WSMessageDTO) {
        session.sendMessage(TextMessage(jacksonObjectMapper().writeValueAsString(msg)))
    }
//get user name from the JWT toke
    fun getUsernameFromToken(token: String): String {
        return getClaimFromToken(token, java.util.function.Function { it.subject })
    }

    fun <T> getClaimFromToken(token: String, claimsResolver: java.util.function.Function<Claims, T>): T {
        val claims = getAllClaimsFromToken(token)
        return claimsResolver.apply(claims)
    }

    private fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parser()
                .setSigningKey(Constant.SIGNING_KEY)
                .parseClaimsJws(token)
                .body
    }
}