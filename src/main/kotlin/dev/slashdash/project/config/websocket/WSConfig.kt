package dev.slashdash.project.config.websocket

import dev.slashdash.project.config.jwt.TokenProvider
import dev.slashdash.project.service.chat.ChatRoomService
import dev.slashdash.project.service.user.UserService
import dev.slashdash.project.util.Constant.API_PATH_PREFIX
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import org.springframework.web.socket.server.HandshakeInterceptor

//web socket configuration
@Configuration
@EnableWebSocket
class WebSocketConfig : WebSocketConfigurer {

    @Autowired
    lateinit var chatRoomService: ChatRoomService

    @Autowired
    lateinit var jwtTokenUtil: TokenProvider

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var authenticationManager: AuthenticationManager
//registers the web socket handlers with required services and managers

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(WSHandlerChatRooms(chatRoomService, jwtTokenUtil, userService, authenticationManager), "/$API_PATH_PREFIX/ws/chat")
                .setAllowedOrigins("*")
                .addInterceptors(HttpSessionIdHandshakeInterceptor())
//                .setHandshakeHandler()
    }




}

