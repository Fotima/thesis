package dev.slashdash.project.config

import dev.slashdash.project.util.Constant.WEB_PATH_PREFIX
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse



class AuthSuccessHandler : SavedRequestAwareAuthenticationSuccessHandler() {
    override fun determineTargetUrl(request: HttpServletRequest, response: HttpServletResponse?): String {
        return "/$WEB_PATH_PREFIX/main"
    }
}