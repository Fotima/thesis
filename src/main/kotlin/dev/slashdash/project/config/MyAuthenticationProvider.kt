package dev.slashdash.project.config

import dev.slashdash.project.exception.InvalidCredentialsException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import java.io.Serializable
//authenticates the authentication data with authenticationManager (Spring Security)
@Component
class MyAuthenticationProvider @Autowired constructor(
        val authenticationManager: AuthenticationManager
) : AuthenticationProvider, Serializable {

    override fun authenticate(authentication: Authentication?): Authentication {

        if (authentication?.principal != null || authentication?.credentials != null) {
            try {
                val username =  authentication.principal.toString()
                val password =  authentication.credentials.toString()
                val res = authenticationManager.authenticate(
                        UsernamePasswordAuthenticationToken(
                                username,
                                password
                        )
                )
                SecurityContextHolder.getContext().authentication = res
                return res
            } catch (e: AuthenticationException) {
                throw InvalidCredentialsException()
            }
        } else
            throw BadCredentialsException("Username and Password are required")
    }

    override fun supports(authentication: Class<out Any>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java.isAssignableFrom(authentication)
    }


}