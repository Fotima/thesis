package dev.slashdash.project.model

import dev.slashdash.project.model.security.User
import java.util.*
import javax.persistence.*

@Entity
class Announcement{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int ?=null

    var title:String?=null
    var description:String?=null
    var date: Date?=null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", nullable = false)
    var author: User?=null
}