package dev.slashdash.project.model.enum
//

//enum class TaskStatus {
//    ASSIGNED,
//    IN_PROCESS,
//    COMPLETED,
//    FAILED
//}
enum class TaskPriority constructor(val code:Int){
    HIGH(2),
    NORMAL(1),
    LOW(0);
    companion object {
        fun fromCode(code: Int): TaskPriority {
            for (status in TaskPriority.values()) {
                if (status.code == code) {
                    return status
                }
            }
            throw UnsupportedOperationException(
                    "The task priority code $code is not supported!"
            )
        }
    }
}



enum class TaskStatus constructor(val code: Int) {
    ASSIGNED(0),
    IN_PROCESS(1),
    COMPLETED(2),
    FAILED(-1);



    companion object {
        fun fromCode(code: Int): TaskStatus {
            for (status in TaskStatus.values()) {
                if (status.code == code) {
                    return status
                }
            }
            throw UnsupportedOperationException(
                    "The task status code $code is not supported!"
            )
        }
    }
}
enum class WorkingHourStatus constructor(val code: Int) {
    ISWORKINGHOUR(1),
    NOTWORKINGHOUR(0),
    NODATA(-1);




    companion object {
        fun fromCode(code: Int): WorkingHourStatus {
            for (status in WorkingHourStatus.values()) {
                if (status.code == code) {
                    return status
                }
            }
            throw UnsupportedOperationException(
                    "The task status code $code is not supported!"
            )
        }
        fun fromStatus(status: WorkingHourStatus): String {
            return if(status == WorkingHourStatus.ISWORKINGHOUR){
                "Is working hours"
            }else if (status == WorkingHourStatus.NOTWORKINGHOUR){
                "Non-working hours"
            }else{
                "No data"
            }

        }

    }

}
enum class NotificationParameter constructor(var value: String) {
    SOUND("default"),
    COLOR("#FFFF00")
}