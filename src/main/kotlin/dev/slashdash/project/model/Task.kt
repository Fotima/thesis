package dev.slashdash.project.model

import com.fasterxml.jackson.annotation.JsonIgnore
import dev.slashdash.project.model.enum.TaskPriority
import dev.slashdash.project.model.enum.TaskStatus
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int? = null

    @NotBlank
    var title: String = ""

    var description: String? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "creator_id", nullable = false)
    lateinit var creator: User

//
//    @ManyToOne(fetch = FetchType.LAZY, optional = true)
//    @JoinColumn(name = "assigned_user_id", nullable = true)
//    var assignedUser: User?=null

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_task_many_to_many",
            joinColumns = [JoinColumn(name = "task_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")])
    var assignedUsers: MutableList<User>? = mutableListOf()

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "assigned_role_id", nullable = true)
    var assignedRole: Role?=null


    @Column(name = "task_placement")
    var taskPlacement: String? = null

    @Column(name = "assigned_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    lateinit var assignedDate: Date

    @Column(name = "created_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
     lateinit var createdDate: Date

    @Column(name = "task_start")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    var taskStart: Date?=null

    @Column(name = "task_end")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    var taskEnd: Date?=null

    var status: TaskStatus =TaskStatus.ASSIGNED

    var priority: TaskPriority =TaskPriority.NORMAL

    //
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "modified_user_id", nullable = true)
    var modifiedBy: User? = null

    var modifiedDate: Date? = null


}