package dev.slashdash.project.model

import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
class Department{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int ?=null

    @NotBlank
    @Column(name = "title")
    @Size(max = 100)
    lateinit var title: String

}