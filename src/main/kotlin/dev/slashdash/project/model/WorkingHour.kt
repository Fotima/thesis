package dev.slashdash.project.model

import java.sql.Time
import java.time.LocalTime
import javax.persistence.*

@Entity
@Table(name = "working_hours")
class WorkingHour{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int ?=null
    lateinit var name: String
     var startTime: Time?=null
     var endTime:Time?=null

    var endTimeIsNextDay:Boolean?=false

    var isOffDay:Boolean = false

}