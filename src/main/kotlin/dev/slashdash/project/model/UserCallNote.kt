package dev.slashdash.project.model

import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*

@Entity
class UserCallNote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int? = null

    @Column(name = "call_time")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    var callTime: Date?=null


    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "calling_user_id", nullable = true)
    lateinit var callingUser: User

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "user_id", nullable = true)
    lateinit var calledUser: User

    var note:String?=null
}