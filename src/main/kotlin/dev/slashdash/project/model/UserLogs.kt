package dev.slashdash.project.model

import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "user_logs")
class UserLogs{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int? = null
    @NotBlank
    var userName:String=""
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    var logDate: Date =Calendar.getInstance().time
    @NotBlank
    var logMessage:String=""

    constructor( userName:String, logMessage:String ): super(){
        this.userName=userName
        this.logMessage=logMessage
        this.logDate=Calendar.getInstance().time
    }
}