package dev.slashdash.project.model.chat

import dev.slashdash.project.model.base.BaseModel
import dev.slashdash.project.model.security.User
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
class InstantMessage : BaseModel(){

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "chat_room_id", nullable = false)
    var chatRoom: ChatRoom? = null

    @ManyToOne(fetch = FetchType.LAZY, optional= false)
    @JoinColumn(name = "from_user_id", nullable = false)
    var fromUser: User?=null

    @Lob
    @Type(type = "text")
    var message: String? = null
}