package dev.slashdash.project.model.chat

import dev.slashdash.project.model.base.BaseModel
import dev.slashdash.project.model.security.User
import java.util.*
import javax.persistence.*

@Entity
class ChatRoom:BaseModel() {


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_chat_rooms_many_to_many",
            joinColumns = [JoinColumn(name = "chat_room_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")])
    var connectedUsers: MutableList<User> = mutableListOf()

    var lastMessageTime: Date?=null
    var lastMessage:String?=null
}