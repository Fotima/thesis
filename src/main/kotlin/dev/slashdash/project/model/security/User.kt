package dev.slashdash.project.model.security


import dev.slashdash.project.model.Department
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.model.base.BaseModel
import dev.slashdash.project.model.enum.WorkingHourStatus
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.lang.Nullable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size


@Entity
@Table(name = "users")
class User : BaseModel() {

    @Column(unique = true, name = "email")
    @NotBlank
    @Email
    lateinit var email: String

    @Column(unique = true, name = "employeeId")
    var employeeId: String? = ""

    @NotBlank
    @Column(name = "firstname")
    @Size(max = 100)
    lateinit var firstname: String

    @NotBlank
    @Column(name = "lastname")
    @Size(max = 100)
    lateinit var lastname: String

    var phoneNumber: String? = null

    @Column(name = "dob")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    var dob: Date? = null

    @NotBlank
    @Column(name = "password")
    @Size(max = 100)
    lateinit var password: String

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "role_id", nullable = true)
    lateinit var role: Role

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "schedule_id", nullable = true)
    var schedule: Schedule? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "department_id", nullable = true)
    var department: Department? = null

    var checkedIn: Boolean? = false


    @Column(name = "firebase_token")
    var firebaseToken: String? = null

    @Column(name = "email_confirmed")
    var emailConfirmed: Boolean? = false

}
