package dev.slashdash.project.model.security

import dev.slashdash.project.model.base.BaseModel
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import javax.persistence.*

@Entity
@Table(name = "permissions")
class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int? = null

    lateinit var name: String

    var description: String? = null

    constructor() : super() {}
    constructor(name: String, description:String? ) : super() {
        this.name = name
        this.description=description
    }
}

