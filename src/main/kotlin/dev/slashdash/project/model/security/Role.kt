package dev.slashdash.project.model.security

import javax.persistence.*


@Entity
@Table(name = "role")
class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    var id: Int? = null

    lateinit var name: String

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_permissions",
            joinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")],
            inverseJoinColumns = [JoinColumn(name = "permission_id", referencedColumnName = "id")]
    )
    var permissions: List<Permission> = emptyList()

    var rank: Int? = null


    constructor() : super()

    constructor(name: String) : super() {
        this.name = name
    }

    constructor(name: String, permissions: MutableList<Permission>) : super() {
        this.name = name
        this.permissions = permissions
    }
    constructor(name: String, rank:Int?) : super() {
        this.name = name
        this.rank=rank
    }

}
