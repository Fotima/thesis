package dev.slashdash.project.model

import org.springframework.data.domain.Page
import java.util.*

class PageWrapper<T>(

) {

    var page: Page<T>? = null
    var url: String? = null
    val MAX_PAGE_ITEM_DISPLAY = 5
    var items: MutableList<PageItem> = mutableListOf()
    var currentNumber: Int = 0


    constructor(page: Page<T>, url: String) : this() {
        this.page = page
        this.url = url
        items = ArrayList()

        currentNumber = page.number + 1 //start from 1 to match page.page

        val start: Int
        val size: Int
        if (page.totalPages <= MAX_PAGE_ITEM_DISPLAY) {
            start = 1
            size = page.totalPages
        } else {
            if (currentNumber <= MAX_PAGE_ITEM_DISPLAY - MAX_PAGE_ITEM_DISPLAY / 2) {
                start = 1
                size = MAX_PAGE_ITEM_DISPLAY
            } else if (currentNumber >= page.totalPages - MAX_PAGE_ITEM_DISPLAY / 2) {
                start = page.totalPages - MAX_PAGE_ITEM_DISPLAY + 1
                size = MAX_PAGE_ITEM_DISPLAY
            } else {
                start = currentNumber - MAX_PAGE_ITEM_DISPLAY / 2
                size = MAX_PAGE_ITEM_DISPLAY
            }
        }

        for (i in 0 until size) {
            items.add(PageItem(start + i, start + i == currentNumber))
        }
    }


    fun getContent(): List<T> {
        return page!!.content
    }

    fun getSize(): Int {
        return page!!.size
    }

    fun getTotalPages(): Int {
        return page!!.totalPages
    }

    fun getTotalElements(): Long {
        return page!!.totalElements
    }

    fun isFirstPage(): Boolean {
        return page!!.isFirst
    }

    fun isLastPage(): Boolean {
        return page!!.isLast
    }

    fun isHasPreviousPage(): Boolean {
        return page!!.hasPrevious()
    }

    fun isHasNextPage(): Boolean {
        return page!!.hasNext()
    }


    inner class PageItem(val number: Int, val isCurrent: Boolean)

}