package dev.slashdash.project.model.base

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull


@MappedSuperclass
abstract class BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Int? = null

    @JsonIgnore
    var active: Boolean = true
    @JsonIgnore
    var deleted: Boolean = false

    @JsonIgnore
    var createdBy: String? = null
    var created: Date? = null
    @JsonIgnore
    var modifiedBy: String? = null
    @JsonIgnore
    var modified: Date? = null

    @PrePersist
    fun onPrePersist() {
        audit()
    }

    @PreUpdate
    fun onPreUpdate() {
        audit()
    }

    @PreRemove
    fun onPreRemove() {
        audit()
    }

    private fun audit() {
        if(id == null){
            created = (Date())
        }
        modified = (Date())
    }
}