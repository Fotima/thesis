package dev.slashdash.project.model

import dev.slashdash.project.model.security.User
import javax.persistence.*

@Entity
class Schedule{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    var id: Int? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "monday_id", nullable = false)
    var mon: WorkingHour? = null


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tueday_id", nullable = false)
    var tues: WorkingHour? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wednesday_id", nullable = false)
    var wed: WorkingHour? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "thursday_id", nullable = false)
    var thur: WorkingHour? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "friday_id", nullable = false)
    var fr: WorkingHour? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sat_id", nullable = false)
    var sat: WorkingHour? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sun_id", nullable = false)
    var sun: WorkingHour? = null




}