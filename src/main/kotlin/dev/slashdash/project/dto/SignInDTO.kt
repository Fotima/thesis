package dev.slashdash.project.dto

import dev.slashdash.project.model.security.User
import java.util.*

class SignInDTO (
        val username : String,
        val password : String
)
class SignInResponseDTO(access_token: String, expire_date: Date, accountConfirmed:Boolean, user: User, hasLowerRoles:Boolean) {
    var access_token : String = access_token
    var expire_date : Date = expire_date
    var accountConfirmed: Boolean = accountConfirmed
    var user:UserAuthDTO?=UserAuthDTO().toDTO(user)
    var hasLowerRoles:Boolean=hasLowerRoles

}