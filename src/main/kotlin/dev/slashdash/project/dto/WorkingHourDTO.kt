package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.WorkingHour
import java.sql.Time
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.validation.constraints.NotBlank

class WorkingHourDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null


    @NotBlank
    var name: String = ""

    var startTime: String = ""

    var endTime: String = ""

    var endTimeIsNextDay: Boolean? = false
    var is_off_day: Boolean = false

    fun toEntity(inputEntity: WorkingHour, startTime: LocalTime?, endTime: LocalTime?): WorkingHour {
        var entity = inputEntity ?: WorkingHour()
        entity.name = name
        if(endTime!=null && startTime!=null){
            entity.endTimeIsNextDay = endTime.isBefore(startTime)
            entity.startTime = Time.valueOf(startTime)
            entity.endTime = Time.valueOf(endTime)

        }else{
            entity.startTime=null
            entity.endTime=null
        }
        entity.isOffDay=inputEntity.isOffDay
        return entity
    }

    fun toDTO(entity: WorkingHour): WorkingHourDTO {
        var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        this.name = entity.name;
        this.id = entity.id
        if (entity.endTime != null) {
            this.endTime = formatter.format(entity.endTime?.toLocalTime())
        }
        if (entity.startTime != null) {
            this.startTime = formatter.format(entity.startTime?.toLocalTime())
        }
        this.endTimeIsNextDay = entity.endTimeIsNextDay
        this.is_off_day = entity.isOffDay
        return this
    }
}
class WorkingHourMobileDTO{
    var name:String?=""
    var startTime:String?=""
    var endTime:String?=""
    fun toDTO(entity: WorkingHour):WorkingHourMobileDTO{
        var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        if (entity.endTime != null) {
            this.endTime = formatter.format(entity.endTime?.toLocalTime())
        }
        if (entity.startTime != null) {
            this.startTime = formatter.format(entity.startTime?.toLocalTime())
        }
        this.name=entity.name
        return this
    }
}