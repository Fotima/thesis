package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.Department
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.model.enum.WorkingHourStatus
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import dev.slashdash.project.util.checkWorkingHours
import dev.slashdash.project.util.getWorkingHour
import org.springframework.security.crypto.bcrypt.BCrypt
import java.io.File.separator
import java.time.LocalTime
import java.util.*
import javax.validation.constraints.NotBlank


class UserCreateDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null


    @NotBlank
    var email: String = ""

    @NotBlank
    var firstName: String = ""

    @NotBlank
    var lastName: String = ""

    @NotBlank
    var password: String = ""

    var role: Int = 0

    var schedule: ScheduleCreateDTO? = null


    var phoneNumber: String? = "-"

    @JsonFormat(pattern = "dd/MM/yyyy")
    var dob: Date? = null
    var employeeId: String? = null

    var departmentId: Int? = null

    fun toEntity(inputEntity: User? = null, role: Role, department: Department?, schedule: Schedule?): User {
        val entity = inputEntity ?: User()
        entity.role = role
        entity.schedule = schedule
        val encodedPass = BCrypt.hashpw(entity.password, BCrypt.gensalt(4))
        entity.password = encodedPass
        entity.department = department

        return entity
    }
}


class UserInfoDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var email: String = ""

    @NotBlank
    var fullName: String = ""

    var rolesString: String = ""

    var phoneNumber: String? = ""

    var hasSchedule: Boolean? = null

    var dob: Date? = null

    var employeeId: String? = null

    var workingHourStatus: String = "";

    var checkedIn: Boolean = false


    fun toDTO(entity: User): UserInfoDTO {
        this.id = entity.id
        this.fullName = "${entity.firstname} ${entity.lastname}"
        this.email = entity.email
        this.phoneNumber = entity.phoneNumber
        this.rolesString = entity.role.name
        this.hasSchedule = entity.schedule != null
        this.dob = entity.dob
        this.employeeId = entity.employeeId
        this.checkedIn = entity.checkedIn ?: false
//        this.workingHourStatus = WorkingHourStatus.fromStatus(checkWorkingHours(entity.schedule))
        this.workingHourStatus = getWorkingHour(entity.schedule)
        return this
    }
}

class UserScheduleInfoDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var fullName: String = ""


    var schedule: ScheduleInfoDTO? = null

    fun toDTO(entity: User): UserScheduleInfoDTO {
        this.id = entity.id
        this.fullName = "${entity.firstname} ${entity.lastname}"

        if (entity.schedule != null) {
            this.schedule = ScheduleInfoDTO().toDTO(entity.schedule!!)

        } else {
            this.schedule = ScheduleInfoDTO()
        }
        return this
    }
}

class UserStatusDTO {
    @NotBlank
    var status: Boolean = false
}

class UserDataUpdateMobileDTO {
    @NotBlank
    var firstName: String = ""

    @NotBlank
    var lastName: String = ""

    var phoneNumber: String? = ""

}


class UserInfoMobileDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var email: String = ""

    @NotBlank
    var firstName: String = ""

    @NotBlank
    var lastName: String = ""

    var phoneNumber: String? = ""

    var schedule: MutableList<WorkingHourMobileDTO>? = mutableListOf<WorkingHourMobileDTO>()

    var dob: Long? = null

    var employeeId: String? = null


    var checkedIn: Boolean = false
    var role: String? = ""


    fun toDTO(entity: User): UserInfoMobileDTO {
        this.id = entity.id
        this.firstName = entity.firstname
        this.lastName = entity.lastname
        this.email = entity.email
        this.phoneNumber = entity.phoneNumber
        if (entity.schedule != null) {
            val mySchedule: MutableList<WorkingHourMobileDTO> = mutableListOf<WorkingHourMobileDTO>()
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.mon!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.tues!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.wed!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.thur!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.fr!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.sat!!))
            mySchedule.add(WorkingHourMobileDTO().toDTO(entity.schedule!!.sun!!))

            this.schedule = mySchedule
        }
        if (entity.dob != null) {
            this.dob = entity.dob?.time
        }
        this.employeeId = entity.employeeId
        this.checkedIn = entity.checkedIn ?: false
        if (entity.role != null) {
            this.role = entity.role.name
        }

        return this
    }
}

class UserAuthDTO{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var firstName: String = ""

    @NotBlank
    var lastName: String = ""
    var role: RoleInfoMobileDTO? = null

    fun toDTO(entity: User): UserAuthDTO {
        this.id = entity.id
        this.firstName = entity.firstname
        this.lastName = entity.lastname
        this.role = RoleInfoMobileDTO().toDTO(entity.role)
        return this
    }
}
class UserShortDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var firstName: String = ""

    @NotBlank
    var lastName: String = ""
    var role: RoleShortDTO? = null
    var phoneNumber: String? = null

    fun toDTO(entity: User): UserShortDTO {
        this.id = entity.id
        this.firstName = entity.firstname
        this.lastName = entity.lastname
        this.role = RoleShortDTO().toDTO(entity.role)
        this.phoneNumber = entity.phoneNumber
        return this
    }
}

class EmailConfirmDTO {
    var email: String = ""
}

class UsersByRoleDTO {
    var role: String? = null
    var users: List<UserShortDTO> = listOf()

    constructor(role: String?, users: List<UserShortDTO>) {
        this.role = role
        this.users = users
    }
}