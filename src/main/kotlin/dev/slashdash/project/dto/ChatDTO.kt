package dev.slashdash.project.dto

import dev.slashdash.project.model.chat.ChatRoom
import dev.slashdash.project.model.chat.InstantMessage
import org.springframework.data.domain.Pageable
import java.time.LocalDateTime
import java.time.ZoneOffset

class ChatRoomInfoDTO {
    var chatId: Int? = null
    var createdAt: Long? = null
    var lastMessageTime: Long? = null
    var listOfUser: List<UserShortDTO>? = null
    var lastMessage:String?=null


    fun toDTO(entity: ChatRoom): ChatRoomInfoDTO {
        this.chatId = entity.id
        this.createdAt = entity.created?.time ?: LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
        this.lastMessageTime = entity.lastMessageTime?.time
        this.listOfUser = entity.connectedUsers.map { UserShortDTO().toDTO(it) }
        this.lastMessage=entity.lastMessage
        return this
    }
}

class ChatInstantMessageDTO {
    var id: Int? = null
    var created: Long? = null
    var fromUser: UserShortDTO? = null
    var message: String? = null

    fun toDTO(entity: InstantMessage): ChatInstantMessageDTO {
        this.id = entity.id
        this.created = entity.created!!.time
        this.fromUser = UserShortDTO().toDTO(entity.fromUser!!)
        this.message = entity.message
        return this
    }
}

class ChatMessageCreateDTO {
    var chatId: Int = 0
    var message: String = ""

    constructor(chatId: Int, message: String) {
        this.chatId = chatId
        this.message = message
    }
}

class ChatMessagePageableDTO {
    var chatRoom: ChatRoomInfoDTO? = null
    var content: List<ChatMessagesByDateDTO>? = null
    var pageable: Pageable? = null
    var totalElements: Long = 0
    var last: Boolean = true

}

class ChatMessagesByDateDTO {
    var date: Long? = null
    var messages: List<ChatInstantMessageDTO>? = listOf()

    constructor(date: Long, messages: List<ChatInstantMessageDTO>) {
        this.date = date
        this.messages = messages
    }
}