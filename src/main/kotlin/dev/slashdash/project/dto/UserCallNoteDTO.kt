package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.UserCallNote
import dev.slashdash.project.model.security.User
import java.util.*

class UserCallNoteInfoDTO {
    var id: Int? = null

    var callTime: Long? = null
    var callingUser: UserShortDTO? = null
    var calledUser: UserShortDTO? = null
    var note: String? = null


    fun toDTO(entity: UserCallNote): UserCallNoteInfoDTO {
        this.id = entity.id
        if(entity.callTime!=null){
            this.callTime = entity.callTime?.time
        }
        this.calledUser = UserShortDTO().toDTO(entity.calledUser)
        this.callingUser = UserShortDTO().toDTO(entity.callingUser)
        this.note = entity.note
        return this
    }
}
class UserCallCreateDTO{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null
    var calledUserId:Int?=null

    fun toEntity(dto:UserCallCreateDTO, callingUser:User, calledUser: User):UserCallNote{
        val entity = UserCallNote()
        entity.callingUser=callingUser
        entity.calledUser=calledUser
        entity.callTime= Calendar.getInstance().time
        return  entity
    }
}
class UserCallNoteCreateDTO{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    var note:String?=null
}