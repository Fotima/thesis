package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.Announcement
import javax.validation.constraints.NotBlank

class AnnouncementInfoDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    var title: String? = null
    var description: String? = null
    var date: Long? = null
    var author: UserShortDTO? = null

    fun toDTO(entity: Announcement): AnnouncementInfoDTO {
        this.id = entity.id
        this.title = entity.title
        this.description = entity.description
        this.date = entity.date?.time
        this.author = if (entity.author != null) UserShortDTO().toDTO(entity.author!!) else null
        return this
    }

}
class AnnouncementCreateDTO{
    var title:String?=null
    var description:String?=null
}