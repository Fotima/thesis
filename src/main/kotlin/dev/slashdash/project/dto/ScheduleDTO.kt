package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.WorkingHour
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.validation.constraints.NotBlank

class ScheduleCreateDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null
    var mon: Int? = null
    var tues: Int? = null
    var wed: Int? = null
    var thur: Int? = null
    var fr: Int? = null
    var sat: Int? = null
    var sun: Int? = null
}

class ScheduleInfoDTO {
    var mon: String = ""
    var tues: String = ""
    var wed: String = ""
    var thur: String = ""
    var fr: String = ""
    var sat: String = ""
    var sun: String = ""

    fun toDTO(entity: Schedule): ScheduleInfoDTO {
        var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        this.mon = setValue(entity.mon!!, formatter)
        this.tues = setValue(entity.tues!!, formatter)
        this.wed = setValue(entity.wed!!, formatter)
        this.thur = setValue(entity.thur!!, formatter)
        this.fr = setValue(entity.fr!!, formatter)
        this.sat = setValue(entity.sat!!, formatter)
        this.sun = setValue(entity.sun!!, formatter)

        return this
    }

    fun setValue(workingHour: WorkingHour, formatter: DateTimeFormatter): String {
        if (workingHour.isOffDay) {
            return workingHour.name
        } else {
            return formatter.format(workingHour.startTime?.toLocalTime()) + " - " + formatter.format(workingHour.endTime?.toLocalTime())

        }
    }
}

class ScheduleMobileInfoDTO {
    var mon: WorkingHourMobileDTO? = null
    var tues: WorkingHourMobileDTO? = null
    var wed: WorkingHourMobileDTO? = null
    var thur: WorkingHourMobileDTO? = null
    var fr: WorkingHourMobileDTO? = null
    var sat: WorkingHourMobileDTO? = null
    var sun: WorkingHourMobileDTO? = null

    fun toDTO(entity: Schedule): ScheduleMobileInfoDTO {
        this.mon = WorkingHourMobileDTO().toDTO(entity.mon!!)
        this.tues = WorkingHourMobileDTO().toDTO(entity.tues!!)
        this.wed = WorkingHourMobileDTO().toDTO(entity.wed!!)
        this.thur = WorkingHourMobileDTO().toDTO(entity.thur!!)
        this.fr = WorkingHourMobileDTO().toDTO(entity.fr!!)
        this.sat = WorkingHourMobileDTO().toDTO(entity.sat!!)
        this.sun = WorkingHourMobileDTO().toDTO(entity.sun!!)

        return this
    }


}
 class UserScheduleDTO{
     var workingHours: List<Date> = listOf()
     var isOffDay:Boolean=false

 }


