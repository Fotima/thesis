package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.Task
import dev.slashdash.project.model.enum.TaskPriority
import dev.slashdash.project.model.enum.TaskStatus
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import java.time.LocalDateTime
import java.util.*
import javax.validation.constraints.NotBlank

class TaskCreateDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var title: String = ""

    var description: String = ""

    var assignedUserIds: MutableList<Int>? = mutableListOf<Int>()

    var assignedRoleId: Int? = null

    var taskPlacement: String? = null

    @NotBlank
    @JsonFormat(pattern = "dd/MM/yyyy")
    var assignedDate: Date = Date()

    @NotBlank
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    var taskStart: Date? = null

    @NotBlank
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    var taskEnd: Date? = null

    var status: Int = 0

    var priority: Int = 0

    fun toEntity(dto: TaskCreateDTO, creatorUser: User, assignedUsers: MutableList<User>?, assginedRole: Role?): Task {
        val entity = Task()
        entity.id = dto.id
        entity.title = dto.title
        entity.description = dto.description
        entity.creator = creatorUser
        entity.assignedUsers = assignedUsers
        entity.assignedRole = assginedRole
        entity.taskPlacement = dto.taskPlacement
        entity.assignedDate = Calendar.getInstance().time
        entity.taskStart = dto.taskStart
        entity.taskEnd = dto.taskEnd
        entity.status = TaskStatus.fromCode(dto.status)
        entity.priority = TaskPriority.fromCode(dto.priority)
        return entity
    }

    fun toDTO(entity: Task, roleId: Int?, userIds: MutableList<Int>?): TaskCreateDTO {
        this.id = entity.id
        this.title = entity.title
        this.description = entity.description ?: ""
        this.assignedUserIds = userIds
        this.assignedRoleId = roleId
        this.taskPlacement = entity.taskPlacement
        this.assignedDate = entity.assignedDate
        this.taskStart = entity.taskStart
        this.taskEnd = entity.taskEnd

        this.status = entity.status.code
        this.priority = entity.priority.code
        return this
    }
}

class TaskStatusUpdateDTO {
    @NotBlank
    var taskId: Int = 0;

    @NotBlank
    var status: Int = 0
}

class TaskInfoDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var title: String = ""

    var description: String = ""

    var assignedUser: List<UserShortDTO>? = null

    var assignedUserNames:List<String>?=null

    var creatorUser: UserShortDTO? = null

    var assignedRole: RoleShortDTO? = null

    var taskPlacement: String? = null


    var assignedDate: Long? = null


    var taskStart: Long? = null

    var taskEnd: Long? = null

    var status: Int = 0

    var priority: Int = 0

    var lastModifiedBy:UserShortDTO?=null
    var lastModifiedDate:Long?=null

    fun toDTO(entity: Task): TaskInfoDTO {
        this.id = entity.id
        this.title = entity.title
        this.description = entity.description ?: ""
        if (entity.assignedUsers != null) {
            this.assignedUser =entity.assignedUsers?.map {   UserShortDTO().toDTO(it) }?.toList()
            this.assignedUserNames=entity.assignedUsers?.map { it.firstname+it.lastname }
        }
        this.creatorUser = UserShortDTO().toDTO(entity.creator)
        if (entity.assignedRole != null) {
            this.assignedRole = RoleShortDTO().toDTO(entity.assignedRole!!)
        }
        this.taskPlacement = entity.taskPlacement
        this.assignedDate = entity.assignedDate.time
        if (entity.taskStart != null) {
            this.taskStart = entity.taskStart!!.time
        }
        if (entity.taskEnd != null) {
            this.taskEnd = entity.taskEnd!!.time
        }
        this.status = entity.status.code
        this.priority = entity.priority.code
        if(entity.modifiedBy!=null){
            this.lastModifiedBy=UserShortDTO().toDTO(entity.modifiedBy!!)
        }
        if(entity.modifiedDate!=null){
            this.lastModifiedDate=entity.modifiedDate?.time
        }
        return this
    }
}