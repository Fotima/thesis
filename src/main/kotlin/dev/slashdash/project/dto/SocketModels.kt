package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.web.socket.WebSocketSession

//class SessionUserName{
//    var session:WebSocketSession?=null
//    var account:String?=null
//
//    constructor(session:WebSocketSession, account:String){
//        this.session=session
//        this.account=account
//    }
//}
data class SessionUserName(
        var session: WebSocketSession,
        var account: String
)
data class WSMessage(
        var type: String="NEW_CHAT_MESSAGE",
        var data: Any
)
class WSMessageDTO(
        var type: String,
        var data: Any?

)