package dev.slashdash.project.dto

import java.util.*

class TaskFilterDTO {
    var fromDate: Long?=null
    var toDate:Long?=null
    var assignedUsers:List<Int>?=null
    var createdUsers:List<Int>?=null
    var myTasks:Boolean=false

}