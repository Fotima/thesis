package dev.slashdash.project.dto

import com.fasterxml.jackson.annotation.JsonInclude
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.model.security.Role
import javax.validation.constraints.NotBlank

class RoleCreateDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var name: String = ""

    var rank: Int? = 0

    var permissions: List<Int> = mutableListOf();


    fun toEntity(inputEntity: Role? = null, permissions: MutableList<Permission>): Role {
        val entity = inputEntity ?: Role()
        entity.permissions = permissions
        return entity
    }
}

class RoleInfoDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var name: String = ""

    @NotBlank
    var rank: Int? = 0

    var permissions: String = "";
    fun toDTO(entity: Role): RoleInfoDTO {
        this.id = entity.id;
        this.name = entity.name;
        this.rank = entity.rank;
        this.permissions = entity.permissions.map { it.name }.toList().joinToString(separator = ", ")
        return this
    }
}


class RoleInfoMobileDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var name: String = ""

    @NotBlank
    var rank: Int? = 0

    var permissions: List<String> = emptyList();

    fun toDTO(entity: Role): RoleInfoMobileDTO {
        this.id = entity.id;
        this.name = entity.name;
        this.rank = entity.rank;
        this.permissions = entity.permissions.map { it.name }.toList()
        return this
    }
}

class RoleShortDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var id: Int? = null

    @NotBlank
    var name: String = ""

    var rank: Int?=null


    fun toDTO(entity: Role): RoleShortDTO {
        this.id = entity.id
        this.name = entity.name
        this.rank=entity.rank
        return this
    }
}