package dev.slashdash.project.service.deparment

import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.Department
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.repo.DepartmentRepo
import dev.slashdash.project.repo.UserLogsRepo
import dev.slashdash.project.service.userLogs.UserLogsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
//Department service
@Service
class DepartmentService : IDepartmentService {
    @Autowired
    lateinit var departmentRepo: DepartmentRepo

    @Autowired
    lateinit var userLogsService: UserLogsService
// get department model by id
    override fun getDepartmentById(id: Int,principal: Principal): Department {
        userLogsService.addLog(UserLogs(principal.name, "Get department $id info"))
        return departmentRepo.findByIdOrNull(id) ?: throw IdNotFoundException()
    }
// get all departments from the server
    override fun getAllDepartments(principal: Principal): List<Department> {
//        userLogsService.addLog(UserLogs(principal.name, "Get list of departments"))
        return departmentRepo.findAll()
    }

//    create or update department on the server
    override fun createUpdateDepartment(department: Department,principal: Principal): Department {
//    if there is department id, get the model data from the server, otherwise, create a new model
        val entity: Department = if (department.id != null) {
            departmentRepo.findByIdOrNull(department.id)
                    ?: throw EntryNotFoundException("Deparment with this ID not found!")
        } else {
            Department()
        }
        entity.title = department.title
        departmentRepo.save(entity)
        userLogsService.addLog(UserLogs(principal.name, "save department ${department.title}"))

        return entity
    }

// delete department from the server
    override fun deleteDepartment(id: Int,principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "delete department $id"))
        return departmentRepo.deleteById(id)
    }

}