package dev.slashdash.project.service.deparment

import dev.slashdash.project.model.Department
import java.security.Principal

interface IDepartmentService {
    fun getDepartmentById(id:Int,principal: Principal):Department
    fun getAllDepartments(principal: Principal):List<Department>
    fun createUpdateDepartment(department: Department,principal: Principal):Department
    fun deleteDepartment(id: Int,principal: Principal)
}