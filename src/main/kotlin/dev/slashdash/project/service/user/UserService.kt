package dev.slashdash.project.service.user

import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.GoogleUserNotFoundException
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.Department
import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.*
import dev.slashdash.project.service.userLogs.UserLogsService
import dev.slashdash.project.util.Constant.ROLE_ADMIN
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.*
import javax.transaction.Transactional

//User service
@Service(value = "userService")
@Transactional
class UserService : UserDetailsService {

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var iUserService: UserService

    @Autowired
    lateinit var roleRepo: RoleRepo

    @Autowired
    lateinit var workingHourRepo: WorkingHourRepo

    @Autowired
    lateinit var scheduleRepo: ScheduleRepo


    @Autowired
    lateinit var departmentRepo: DepartmentRepo

    @Autowired
    private val bcryptEncoder: BCryptPasswordEncoder? = null

    @Autowired
    lateinit var userLogsService: UserLogsService
//load user data by user name
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        userLogsService.addLog(UserLogs(username, "Log in request"))

        val user = userRepo.findByEmail(username)
                ?: throw UsernameNotFoundException("Invalid username or password.")
        return org.springframework.security.core.userdetails.User(user.email, user.password,
                getAuthorities(user.role)
        )
    }

    private fun getAuthorities(
            role: Role): Collection<GrantedAuthority?>? {
        return getGrantedAuthorities(getPermissions(role))
    }
// get list of permissions of user's role
    private fun getPermissions(role: Role): List<String>? {
        val privileges: MutableList<String> = ArrayList()
        val collection: List<Permission> = role.permissions

        for (item in collection) {
            privileges.add(item.name)
        }
        return privileges
    }

    private fun getGrantedAuthorities(privileges: List<String>?): List<GrantedAuthority>? {
        val authorities: MutableList<GrantedAuthority> = ArrayList()
        if (privileges != null) {
            for (privilege in privileges) {
                authorities.add(SimpleGrantedAuthority(privilege))
            }
        }

        return authorities
    }

    private fun getAuthority(user: User): Collection<SimpleGrantedAuthority> {
        val authorities = ArrayList<SimpleGrantedAuthority>()
        authorities.add(SimpleGrantedAuthority("ROLE_" + user.role.name))
        return authorities
    }

// get user by id
    fun getUserById(id: Int, principal: Principal): UserInfoDTO {
        userLogsService.addLog(UserLogs(principal.name, "Get user info by id=$id"))

        return UserInfoDTO().toDTO(userRepo.findByIdOrNull(id)
                ?: throw  EntryNotFoundException("User with this ID is not found"))
    }

    fun getUser(id: Int, principal: Principal): User {
        userLogsService.addLog(UserLogs(principal.name, "Get user info by id=$id"))

        return userRepo.findByIdOrNull(id)
                ?: throw  EntryNotFoundException("User with this ID is not found")
    }
//create or update user on the server
    fun createOrUpdateUser(dto: UserCreateDTO, principal: Principal): UserShortDTO {
        var roles = roleRepo.findByIdOrNull(dto.role) ?: throw IdNotFoundException()

        var deparment: Department? = if (dto.departmentId != null) {
            departmentRepo.findByIdOrNull(dto.departmentId)
                    ?: throw IdNotFoundException()
        } else {
            null
        }
        val entity: User = if (dto.id != null) {
            userRepo.findByIdOrNull(dto.id)
                    ?: throw EntryNotFoundException("User with this ID not found!")
//            dto.toEntity(existingEntity, roles, )
        } else {
            User();
        }
        entity.firstname = dto.firstName
        entity.lastname = dto.lastName
        entity.password = dto.password
        entity.email = dto.email
        entity.phoneNumber = dto.phoneNumber
        entity.schedule = entity.schedule
        entity.dob = dto.dob
        entity.employeeId = dto.employeeId
        dto.toEntity(entity, roles, deparment, entity.schedule);
        userRepo.save(entity)
        userLogsService.addLog(UserLogs(principal.name, "Save user ${entity.toString()}"))

        return UserShortDTO().toDTO(entity)
    }
//update user schedule on the server
    fun updateUserSchedule(dto: ScheduleCreateDTO, userId: Int, principal: Principal): User {
        val entity: User =
                userRepo.findByIdOrNull(userId)
                        ?: throw EntryNotFoundException("User with this ID not found!")

        var schedule: Schedule
        if (entity.schedule == null) {
            schedule = Schedule()
        } else {
            schedule = entity.schedule!!
        }
        var workingHour: WorkingHour
        workingHour = workingHourRepo.findByIdOrNull(dto.mon)
                ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
        schedule.mon = workingHour
        if (dto.tues == workingHour.id) {
            schedule.tues = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.tues)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.tues = workingHour
        }

        if (dto.wed == workingHour.id) {
            schedule.wed = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.wed)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.wed = workingHour
        }
        if (dto.thur == workingHour.id) {
            schedule.thur = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.thur)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.thur = workingHour
        }
        if (dto.fr == workingHour.id) {
            schedule.fr = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.fr)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.fr = workingHour
        }
        if (dto.sat == workingHour.id) {
            schedule.sat = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.sat)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.sat = workingHour
        }

        if (dto.sun == workingHour.id) {
            schedule.sun = workingHour
        } else {
            workingHour = workingHourRepo.findByIdOrNull(dto.sun)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")
            schedule.sun = workingHour
        }
        scheduleRepo.save(schedule)
        entity.schedule = schedule
        userRepo.save(entity)
        userLogsService.addLog(UserLogs(principal.name, "Save user $userId shedule ${dto.toString()}"))

        return entity
    }


    fun getAllUsersWeb(pageable: Pageable, principal: Principal): Page<UserInfoDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get All Users"))

        return userRepo.findAll(pageable).map { UserInfoDTO().toDTO(it) }
    }

    fun getAllUsers(principal: Principal): List<UserShortDTO> {
        return userRepo.findAllByEmailNotAndRoleNameNotOrderByFirstname(principal.name, ROLE_ADMIN).map { UserShortDTO().toDTO(it) }

    }

    fun getAllCheckinUsers(principal: Principal): List<UserInfoDTO> {
//        userLogsService.addLog(UserLogs(principal.name, "Get All Users"))
        return userRepo.findTop10ByCheckedInTrue().map { UserInfoDTO().toDTO(it) }
    }

    fun getAllUsersSchedule(pageable: Pageable, principal: Principal): Page<UserScheduleInfoDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get All Users"))

        return userRepo.findAll(pageable).map { UserScheduleInfoDTO().toDTO(it) }
    }

    fun deleteUser(id: Int, principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "Delete user with id=$id"))
        return userRepo.deleteById(id)
    }

    fun checkIfEmailIsUnique(email: String, userId: Int?): Boolean {
        var user = userRepo.findByEmail(email)
        if (user != null) {
            return (userId != null && user.id == userId)
        } else {
            return true
        }
    }

    fun checkIfEmployeeIdIsUnique(employeeId: String?, userId: Int?): Boolean {
        if (employeeId == null) {
            return true
        }
        var user = userRepo.findByEmployeeId(employeeId)
        if (user.isPresent) {
            return (userId != null && user.get().id == userId)
        } else {
            return true
        }
    }

    fun updateUserStatus(userStatusDTO: UserStatusDTO, userEmail: String) {
        var user: User? = userRepo.findByEmail(userEmail)
        if (user != null) {
            user.checkedIn = userStatusDTO.status
            userLogsService.addLog(UserLogs(userEmail, "User check in ${userStatusDTO.status}"))

            userRepo.save(user)
        }
    }

    fun getLowerRankUsers(userEmail: String): List<UserShortDTO> {
        var user: User? = userRepo.findByEmail(userEmail)
        if (user != null) {
            userLogsService.addLog(UserLogs(userEmail, "Get users with lower rank role"))
            return userRepo.findAllByRoleRankIsGreaterThan(user.role.rank!!).map { UserShortDTO().toDTO(it) }
        } else {
            return emptyList()
        }
    }
//save firabse token of a user
    fun saveFirebaseToken(userName: String, token: String): Boolean {
        var user: User = userRepo.findByEmail(userName)
                ?: throw   EntryNotFoundException("User with this username is not found")
        user.firebaseToken = token
        return user.let { userRepo.save(it).id } != null
    }

    fun confirmUserEmail(confirmedEmail: String, userEmail: String) {
        var user: User = userRepo.findByEmail(userEmail)
                ?: throw   EntryNotFoundException("User with this username is not found")
        if (user.email != confirmedEmail) {
            throw   GoogleUserNotFoundException()
        } else {
            user.emailConfirmed = true
            userRepo.save(user);
        }

    }

    fun getUserContacts(principal: Principal): List<UsersByRoleDTO> {
        var groupedList = userRepo.findAllByEmailNotAndRoleNameNotOrderByFirstname(principal.name, ROLE_ADMIN).map { UserShortDTO().toDTO(it) }.groupBy {
            it.role?.name
        }
        val results = groupedList.map { UsersByRoleDTO(it.key, it.value) }
        return results
    }
    fun getUserContactsRank(principal: Principal): List<UsersByRoleDTO> {
        val user = userRepo.findByEmail(principal.name) ?: throw  IdNotFoundException()
        var groupedList = userRepo.findAllByIdAndRoleRankIsGreaterThanOrRoleRankAndRoleNameNotOrderByFirstname(user.id, user.role.rank,user.role.rank, ROLE_ADMIN).map { UserShortDTO().toDTO(it) }.groupBy {
            it.role?.name
        }
        val results = groupedList.map { UsersByRoleDTO(it.key, it.value) }
        return results
    }

    fun getMyProfileData(principal: Principal): UserInfoMobileDTO {
        var user: User = userRepo.findByEmail(principal.name)
                ?: throw   EntryNotFoundException("User with this username is not found")
        return UserInfoMobileDTO().toDTO(user)
    }
//update profile data
    fun updateMyProfileData(principal: Principal, dto: UserDataUpdateMobileDTO): UserInfoMobileDTO {
        var user: User = userRepo.findByEmail(principal.name)
                ?: throw   EntryNotFoundException("User with this username is not found")
        user.phoneNumber = dto.phoneNumber
        user.firstname = dto.firstName
        user.lastname = dto.lastName
        userRepo.save(user)
        return UserInfoMobileDTO().toDTO(user)
    }


}