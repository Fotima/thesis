package dev.slashdash.project.service.user_call_note

import dev.slashdash.project.dto.UserCallCreateDTO
import dev.slashdash.project.dto.UserCallNoteCreateDTO
import dev.slashdash.project.dto.UserCallNoteInfoDTO
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.repo.UserCallNoteRepo
import dev.slashdash.project.repo.UserRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
//User Call Note service
@Service
class UserCallNotService {
    @Autowired
    lateinit var noteRepo: UserCallNoteRepo

    @Autowired
    lateinit var userRepo: UserRepo
// get all notes from the server
    fun getMyCallNotes(principal: Principal): List<UserCallNoteInfoDTO> {
        return noteRepo.getAllByCallingUserEmail(principal.name).map { UserCallNoteInfoDTO().toDTO(it) }
    }

//    create call history on the server
    fun createCallHistory(principal: Principal, dto: UserCallCreateDTO): UserCallNoteInfoDTO {
        val callingUser = userRepo.findByEmail(principal.name) ?: throw IdNotFoundException()
        val calledUser = userRepo.findByIdOrNull(dto.calledUserId) ?: throw IdNotFoundException()
        val entity = UserCallCreateDTO().toEntity(dto, callingUser, calledUser)
        noteRepo.save(entity)
        return UserCallNoteInfoDTO().toDTO(entity)
    }
//     add call note to call history
    fun addNoteToCall(principal: Principal, dto: UserCallNoteCreateDTO): UserCallNoteInfoDTO {
        val callHistory = noteRepo.findByIdOrNull(dto.id) ?: throw IdNotFoundException()
        callHistory.note=dto.note
        noteRepo.save(callHistory)
        return UserCallNoteInfoDTO().toDTO(callHistory)
    }
//    delete note
    fun deleteNote(id: Int) {
        noteRepo.deleteById(id)
    }
}