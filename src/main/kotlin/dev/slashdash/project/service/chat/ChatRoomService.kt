package dev.slashdash.project.service.chat

import dev.slashdash.project.config.websocket.WSHandlerChatRooms
import dev.slashdash.project.dto.*
import dev.slashdash.project.exception.CustomForbiddenException
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.model.chat.ChatRoom
import dev.slashdash.project.model.chat.InstantMessage
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.repo.chat.ChatInstantMessageRepo
import dev.slashdash.project.repo.chat.ChatRoomRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.socket.WebSocketSession
import java.security.Principal
import java.util.*
import kotlin.collections.ArrayList
//Chat room service
@Service
class ChatRoomService {
    @Autowired
    lateinit var chatRoomRepo: ChatRoomRepo

    @Autowired
    lateinit var chatInstantMessageRepo: ChatInstantMessageRepo

    @Autowired
    lateinit var userRepo: UserRepo

//create a new message on the db
    fun createMessage(dto: ChatMessageCreateDTO, principal: Principal): MutableMap<String, Boolean> {
        val result = mutableMapOf<String, Boolean>()
//    find user object by email
        val fromUser = userRepo.findByEmail(principal.name)
                ?: throw EntryNotFoundException("Can't find user  in the system")
//    get the chatroom by id
        val chatRoom = chatRoomRepo.findByIdOrNull(dto.chatId)
                ?: throw EntryNotFoundException("Can't find chat room  in the system")
//    instantiate a new InstantMessage object
        val entity = InstantMessage()
        entity.chatRoom = chatRoom
        entity.fromUser = fromUser
        entity.message = dto.message
        entity.createdBy = principal.name
        entity.modifiedBy = principal.name
        chatInstantMessageRepo.save(entity)
        result["result"] = true
        chatRoom.lastMessageTime = Date()
        chatRoom.lastMessage=dto.message
//    save an object on db
        chatRoomRepo.save(chatRoom)
//    send notification ws service
        sendNotificationToWS(dto.chatId, entity)
        return result

    }


//get chat room data by id
    fun getChatRoomById(chatRoomId: Int, userId: Int): ChatRoomInfoDTO {
        val chatRoom = chatRoomRepo.findByIdOrNull(chatRoomId)
                ?: throw EntryNotFoundException("Can't find chat room  in the system")
        checkIfUserBelongToChat(chatRoom, userId)
        return ChatRoomInfoDTO().toDTO(chatRoom)
    }
//returns list of chats of user by given user email
    fun getAllChatsOfUser(principal: Principal): List<ChatRoomInfoDTO> {
        val fromUser = userRepo.findByEmail(principal.name)
                ?: throw EntryNotFoundException("Can't find user  in the system")
        val result = chatRoomRepo.findByConnectedUsersContainsOrderByLastMessageTimeDesc(fromUser)
        return result.map { ChatRoomInfoDTO().toDTO(it) }
    }

//creates a new chat room with two user ids
    fun createNewChatRoom(withUserId: Int, principal: Principal): ChatRoomInfoDTO {
        val fromUser = userRepo.findByEmail(principal.name)
                ?: throw EntryNotFoundException("Can't find user  in the system")
        val withUser = userRepo.findByIdOrNull(withUserId)
                ?: throw EntryNotFoundException("Can't find user  in the system")
        val users = listOf(withUser, fromUser)
        val chatRoom = chatRoomRepo.findByConnectedUsersContainsAndConnectedUsersContains(fromUser, withUser)
                ?: createNewChat(users)
        return ChatRoomInfoDTO().toDTO(chatRoom)
    }

    fun createNewChat(users: List<User>): ChatRoom {
        val entity = ChatRoom()
        entity.connectedUsers.addAll(users)
        entity.lastMessageTime = Date()

        return chatRoomRepo.save(entity)
    }
//get pageable of messages of a message room grouped by date
    fun getChatMessagesPaged(chatRoomId: Int?, withUserId: Int?, principal: Principal, pageable: Pageable): ChatMessagePageableDTO {
        val user = userRepo.findByEmail(principal.name)
                ?: throw EntryNotFoundException("Can't find user  in the system")
        val chatRoom = when {
//            get chat room by id
            chatRoomId != null -> chatRoomRepo.findByIdOrNull(chatRoomId)
//            get user by id
            withUserId != null -> {
                val withUser = userRepo.findByIdOrNull(withUserId)
                        ?: throw EntryNotFoundException("Can't find user  in the system")
                chatRoomRepo.findByConnectedUsersContainsAndConnectedUsersContains(user, withUser)
            }
            else -> null

        }
//    if chat room is not, add message to consequent date on db
        return if (chatRoom != null) {
            checkIfUserBelongToChat(chatRoom, user.id!!)
            val page = chatInstantMessageRepo.findAllByChatRoom_IdOrderByCreatedBy(chatRoom.id!!, pageable)
            val groupedListDTO = page.content.map { ChatInstantMessageDTO().toDTO(it) }.groupBy {
                val createdCal = Calendar.getInstance()
                createdCal.timeInMillis = it.created!!
                createdCal.set(Calendar.HOUR_OF_DAY, 0)
                createdCal.set(Calendar.MINUTE, 0)
                createdCal.set(Calendar.SECOND, 0)
                createdCal.set(Calendar.MILLISECOND, 0)
                createdCal.timeInMillis
            }
            val results = groupedListDTO.map { ChatMessagesByDateDTO(it.key, it.value) }
            val resutPageable = ChatMessagePageableDTO()
            resutPageable.chatRoom = ChatRoomInfoDTO().toDTO(chatRoom)
            resutPageable.content = results
            resutPageable.pageable = page.pageable
            resutPageable.totalElements = page.totalElements
            resutPageable.last = page.isLast
            return resutPageable
        } else {
//            else add a new list with a new date to the new chat room
            val resutPageable = ChatMessagePageableDTO()
            resutPageable.chatRoom = ChatRoomInfoDTO()
            resutPageable.content = listOf()
            resutPageable.pageable = pageable
            resutPageable.totalElements = 0
            resutPageable.last = true

            return resutPageable
        }
    }
//check if current user is in the list of users of a chat room
    fun checkIfUserBelongToChat(chatRoom: ChatRoom, userId: Int) {
        var hasUserInChat = false
        chatRoom.connectedUsers.forEach {
            if (it.id == userId) {
                hasUserInChat = true
            }
        }
        if (!hasUserInChat)
            throw  CustomForbiddenException("Cannot access chat, to which you not belong")
    }
//create a message on ws service
    fun createMessageFromWS(dto: ChatMessageCreateDTO, fromUser: String): Boolean {
        val fromUser = userRepo.findByEmail(fromUser)
                ?: throw  throw EntryNotFoundException("Can't find user  in the system")
        val chatRoom = chatRoomRepo.findByIdOrNull(dto.chatId)
                ?: throw EntryNotFoundException("Can't find chatroom  in the system")
        val entity = InstantMessage()
        entity.chatRoom = chatRoom
        entity.fromUser = fromUser
        entity.message = dto.message
        entity.createdBy = fromUser.email
        chatInstantMessageRepo.save(entity)
        chatRoom.lastMessageTime = Date()
        chatRoom.lastMessage=dto.message

        chatRoomRepo.save(chatRoom)
        sendNotificationToWS(dto.chatId, entity)
        return true
    }
//send notification to ws server
    fun sendNotificationToWS(chatRoomId: Int, entity: InstantMessage) {
        val sessions = WSHandlerChatRooms.newSession[chatRoomId.toString()]?.map { it.session }
        if (sessions != null) {
            val sessionForMobile = ArrayList<WebSocketSession>()
            sessions.forEach {
                    if (it.isOpen) {
                        sessionForMobile.add(it)
                    } else {
                        System.out.println("session with Id ${it.id} is not open")
                    }

            }
            System.out.println("Opened mobile chat sessions count:${sessionForMobile.size}")
            val messageDTO=ChatInstantMessageDTO().toDTO(entity)
            val message=WSMessageDTO("NEW_CHAT_MESSAGE",messageDTO)
            WSHandlerChatRooms(null, null,null, null).broadcastToSpecificUsers(sessions, message)
        }
    }

}