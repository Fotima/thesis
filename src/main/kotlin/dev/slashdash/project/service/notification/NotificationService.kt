package dev.slashdash.project.service.notification

import com.google.firebase.messaging.*
import dev.slashdash.project.model.enum.NotificationParameter
import dev.slashdash.project.model.security.User
import dev.slashdash.project.util.NotificationUtils
import org.springframework.stereotype.Service
import java.time.Duration
//Notification service configuration file
@Service
class NotificationService{
    fun sendNotificationToMultipleDevices(
            notification: Notification,
            data: Map<String, String>,
            tokens: List<String>)
    {
        // Create a list containing up to 100 registration tokens.
        // These registration tokens come from the client FCM SDKs.

        val message = MulticastMessage.builder()

                .putAllData(data)
                .setNotification(notification)
                .setApnsConfig(getApnsConfig(data.getValue("id")))
                .setAndroidConfig(getAndroidConfig(data.getValue("id")))
                .addAllTokens(tokens)
                .build()

        val response = FirebaseMessaging.getInstance().sendMulticast(message)
    }


    private fun getAndroidConfig(topic: String): AndroidConfig {
        return AndroidConfig.builder()
                .setTtl(Duration.ofHours(1).toMillis()).setCollapseKey(topic)  // Message Time-to-live lifetime! set 1 hour can be changed thou
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder().setSound(NotificationParameter.SOUND.value)
                        .setColor(NotificationParameter.COLOR.value).setTag(topic).build()).build()
    }



    private fun getApnsConfig(topic: String): ApnsConfig {
        return ApnsConfig.builder()
                .setAps(Aps.builder().setCategory(topic).setThreadId(topic).build()).build()
    }




    fun sendNotificationsToUsers(tokens: List<String>, id: Int, code: Int){

        val notification = NotificationUtils.getNotificationFromCode(code)
        val data = mutableMapOf<String, String>()
        data["id"] = id.toString()
        data["click_action"] = "FLUTTER_NOTIFICATION_CLICK"

        sendNotificationToMultipleDevices(
                notification,
                data,
                tokens
        )
    }
}