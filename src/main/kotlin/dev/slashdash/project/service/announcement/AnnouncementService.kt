package dev.slashdash.project.service.announcement

import dev.slashdash.project.dto.AnnouncementCreateDTO
import dev.slashdash.project.dto.AnnouncementInfoDTO
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.Announcement
import dev.slashdash.project.repo.AnnouncementRepo
import dev.slashdash.project.repo.UserRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.*
//Announcement service
@Service
class AnnouncementService {
    @Autowired
    lateinit var announcementRepo: AnnouncementRepo

    @Autowired
    lateinit var userRepo: UserRepo
//returns mapped list of announcements from the db
    fun getAllAnnouncements(): List<AnnouncementInfoDTO> {
        return announcementRepo.findAllByOrderByDateDesc().map { AnnouncementInfoDTO().toDTO(it) }
    }
//    creates a new announcement in the server and return mapped db item
    fun createAnnouncement(dto: AnnouncementCreateDTO, principal: Principal):AnnouncementInfoDTO{
        val user=userRepo.findByEmail(principal.name)?:throw  IdNotFoundException()
        val entity=Announcement()
        entity.title=dto.title
        entity.description=dto.description
        entity.author=user
        entity.date= Date()
        announcementRepo.save(entity)
        return AnnouncementInfoDTO().toDTO(entity)
    }
}