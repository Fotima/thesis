package dev.slashdash.project.service.userLogs

import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.repo.UserLogsRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
//User logs service
@Service
class UserLogsService :IUserLogsService{
    @Autowired
    lateinit var userLogsRepo: UserLogsRepo
    override fun addLog(log: UserLogs) {
      userLogsRepo.save(log);
    }
//get pageable of logs for the latest week
    override fun getAllLogsForLastWeek(pageable: Pageable): Page<UserLogs> {
        val date = LocalDateTime.now().minusDays(7)
     return   userLogsRepo.getAllByLogDateIsAfterOrderByLogDateDesc(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()), pageable)
    }
}