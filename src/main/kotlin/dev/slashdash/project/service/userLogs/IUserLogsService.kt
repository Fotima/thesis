package dev.slashdash.project.service.userLogs

import dev.slashdash.project.model.UserLogs
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface IUserLogsService {
    fun addLog(log:UserLogs)
    fun getAllLogsForLastWeek(pageable: Pageable): Page<UserLogs>

}