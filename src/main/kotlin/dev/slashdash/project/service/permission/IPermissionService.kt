package dev.slashdash.project.service.permission

import dev.slashdash.project.model.security.Permission
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.security.Principal


interface IPermissionService {
    fun getAllPermissions(pageable: Pageable, principal: Principal): Page<Permission>
    fun createPermission(permission: Permission,principal: Principal):Permission
    fun deletePermission(id:Int, principal: Principal)
}