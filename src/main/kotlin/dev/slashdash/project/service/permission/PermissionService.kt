package dev.slashdash.project.service.permission

import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.security.Permission
import dev.slashdash.project.repo.PermissionRepo
import dev.slashdash.project.service.userLogs.UserLogsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
//Permission service
@Service
class PermissionService: IPermissionService{
    @Autowired
    lateinit var permissionRepo: PermissionRepo

    @Autowired
    lateinit var userLogsService: UserLogsService
//returns all permissions from the server
override fun getAllPermissions(pageable: Pageable,principal: Principal): Page<Permission> {
//        userLogsService.addLog(UserLogs(principal.name, "Get all permissions"))
        return permissionRepo.findAll(pageable)
    }
//create permission
    override fun createPermission(permission: Permission,principal: Principal): Permission {
//        userLogsService.addLog(UserLogs(principal.name, "save  permission ${permission.toString()}"))

        permissionRepo.save(permission)
        return permission
    }
//    update existing permission on the server
     fun updatePermission(dto: Permission,principal: Principal): Permission {
//        userLogsService.addLog(UserLogs(principal.name, "save  permission ${permission.toString()}"))
        val permission=permissionRepo.findByIdOrNull(dto.id )?:throw IdNotFoundException()
         permission.description=dto.description
         permission.name=dto.name
        permissionRepo.save(permission)
        return permission
    }
//delete permission from the server
    override fun deletePermission(id: Int, principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "Delete Permission  with id=$id"))
        permissionRepo.deleteById(id)
    }

}