package dev.slashdash.project.service.workingHour

import dev.slashdash.project.dto.WorkingHourDTO
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.WorkingHour
import dev.slashdash.project.repo.WorkingHourRepo
import dev.slashdash.project.service.userLogs.UserLogsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
import java.time.LocalTime
import java.time.format.DateTimeFormatter

// Working hours service
@Service
class WorkingHourService : IWorkingHourService {
    @Autowired
    lateinit var workingHourRepo: WorkingHourRepo

    @Autowired
    lateinit var userLogsService: UserLogsService

    //create or update working hour on the server
    override fun createOrUpdateWorkingHour(dto: WorkingHourDTO, principal: Principal): WorkingHour {
        val startTime: LocalTime? = if (dto.startTime != null && dto.startTime.isNotEmpty()) {
            LocalTime.parse(dto.startTime.trim(), DateTimeFormatter.ofPattern("HH:mm"))
        } else {
            null
        }

        val endTime: LocalTime? = if (dto.endTime != null && dto.endTime.isNotEmpty()) {
            LocalTime.parse(dto.endTime.trim(), DateTimeFormatter.ofPattern("HH:mm"))
        } else {
            null
        }
        val entity: WorkingHour = if (dto.id != null) {
            workingHourRepo.findByIdOrNull(dto.id)
                    ?: throw EntryNotFoundException("WorkingHour with this ID not found!")

        } else {
            WorkingHour()
        }
//        entity.isOffDay = dto.is_off_day
        entity.isOffDay = startTime == null && endTime == null
        entity.name = dto.name
        dto.toEntity(entity, startTime, endTime)
        userLogsService.addLog(UserLogs(principal.name, "Save working hour ${entity}"))

        workingHourRepo.save(entity)
        return entity
    }

    override fun getAllWorkingHours(principal: Principal): List<WorkingHourDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get all Working hours"))

        return workingHourRepo.findAll().map { WorkingHourDTO().toDTO(it) };
    }

    // delete working hour from the server
    override fun deleteWorkingHour(id: Int, principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "Delete working hour $id"))

        workingHourRepo.deleteById(id)
    }

    override fun getWorkingHourById(id: Int, principal: Principal): WorkingHourDTO {
        userLogsService.addLog(UserLogs(principal.name, "Get working hour info $id"))
        return WorkingHourDTO().toDTO(workingHourRepo.findByIdOrNull(id) ?: throw IdNotFoundException())


    }


}