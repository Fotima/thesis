package dev.slashdash.project.service.workingHour

import dev.slashdash.project.dto.WorkingHourDTO
import dev.slashdash.project.model.WorkingHour
import java.security.Principal

interface IWorkingHourService {
    fun createOrUpdateWorkingHour(dto:WorkingHourDTO, principal: Principal):WorkingHour
    fun getAllWorkingHours(principal: Principal):List<WorkingHourDTO>
    fun deleteWorkingHour(id:Int,principal: Principal)
    fun getWorkingHourById(id:Int,principal: Principal):WorkingHourDTO
}