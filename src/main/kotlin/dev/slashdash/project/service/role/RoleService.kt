package dev.slashdash.project.service.role

import dev.slashdash.project.dto.RoleCreateDTO
import dev.slashdash.project.dto.RoleInfoDTO
import dev.slashdash.project.dto.RoleInfoMobileDTO
import dev.slashdash.project.dto.RoleShortDTO
import dev.slashdash.project.exception.EntryNotFoundException
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.PermissionRepo
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.service.userLogs.UserLogsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
import javax.management.relation.RoleInfo
//Role service
@Service
class RoleService : IRoleService {
    @Autowired
    lateinit var roleRepo: RoleRepo

    @Autowired
    lateinit var permissionRepo: PermissionRepo

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var userLogsService: UserLogsService
//returns all roles from the server
    override fun getAllRoles(pageable: Pageable,principal: Principal): Page<RoleInfoDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get all roles"))

        return roleRepo.findAll(pageable).map { RoleInfoDTO().toDTO(it) }
    }
    override fun getAllRolesMobile(pageable: Pageable,principal: Principal): Page<RoleInfoMobileDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get all roles"))

        return roleRepo.findAll(pageable).map { RoleInfoMobileDTO().toDTO(it) }
    }
//creates or updates role in the server
    override fun createOrUpdateRole(dto: RoleCreateDTO,principal: Principal): RoleInfoDTO {
        var permissions = permissionRepo.findAllById(dto.permissions)
        val entity: Role = if (dto.id != null) {
            roleRepo.findByIdOrNull(dto.id)
                    ?: throw EntryNotFoundException("Role with this ID not found!")
        } else {
            Role()
        }
        entity.name = dto.name
        entity.rank = dto.rank
        entity.permissions = permissions
        roleRepo.save(entity)
        userLogsService.addLog(UserLogs(principal.name, "Save roles ${entity.toString()}"))

        return RoleInfoDTO().toDTO(entity)
    }
// returns role model by id
    override fun getRoleById(id: Int,principal: Principal): Role {
        userLogsService.addLog(UserLogs(principal.name, "Get info of Role with id=$id"))

        return roleRepo.findByIdOrNull(id) ?: throw IdNotFoundException()
    }
// deletes role from the server
    override fun deleteRole(id: Int,principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "Delete role with id =$id"))

        return roleRepo.deleteById(id);
    }
// returns all roles with equal or lower rank
    override fun getAllRolesOfLowerRank(userName: String,principal: Principal): List<RoleShortDTO> {
        var user: User? = userRepo.findByEmail(userName)
        userLogsService.addLog(UserLogs(principal.name, "Get all roles of lower rank"))

        if (user != null) {
            return roleRepo.findAllByRankGreaterThan(user.role.rank!!).map { RoleShortDTO().toDTO(it) };
        } else {
            return emptyList()
        }
    }


}