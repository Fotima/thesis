package dev.slashdash.project.service.role

import dev.slashdash.project.dto.RoleCreateDTO
import dev.slashdash.project.dto.RoleInfoDTO
import dev.slashdash.project.dto.RoleInfoMobileDTO
import dev.slashdash.project.dto.RoleShortDTO
import dev.slashdash.project.model.security.Role
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.nio.file.attribute.GroupPrincipal
import java.security.Principal

interface IRoleService  {
    fun getAllRoles(pageable: Pageable, principal: Principal): Page<RoleInfoDTO>
    fun getAllRolesMobile(pageable: Pageable,principal: Principal): Page<RoleInfoMobileDTO>
    fun createOrUpdateRole(dto: RoleCreateDTO,principal: Principal):RoleInfoDTO
    fun getRoleById(id:Int,principal: Principal):Role
    fun deleteRole(id:Int,principal: Principal)
    fun getAllRolesOfLowerRank(userName:String,principal: Principal):List<RoleShortDTO>
}