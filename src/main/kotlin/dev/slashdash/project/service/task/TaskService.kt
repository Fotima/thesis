package dev.slashdash.project.service.task

import dev.slashdash.project.dto.TaskCreateDTO
import dev.slashdash.project.dto.TaskFilterDTO
import dev.slashdash.project.dto.TaskInfoDTO
import dev.slashdash.project.dto.TaskStatusUpdateDTO
import dev.slashdash.project.exception.IdNotFoundException
import dev.slashdash.project.model.Task
import dev.slashdash.project.model.UserLogs
import dev.slashdash.project.model.enum.TaskStatus
import dev.slashdash.project.model.enum.WorkingHourStatus
import dev.slashdash.project.model.security.Role
import dev.slashdash.project.model.security.User
import dev.slashdash.project.repo.RoleRepo
import dev.slashdash.project.repo.TaskRepo
import dev.slashdash.project.repo.UserRepo
import dev.slashdash.project.service.notification.NotificationService
import dev.slashdash.project.service.userLogs.UserLogsService
import dev.slashdash.project.util.NotificationUtils
import dev.slashdash.project.util.checkWorkingHours
import dev.slashdash.project.util.getWorkingTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.security.Principal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.util.*
//Task service
@Service
class TaskService : ITaskService {
    @Autowired
    lateinit var taskRepo: TaskRepo

    @Autowired
    lateinit var userRepo: UserRepo

    @Autowired
    lateinit var roleRepo: RoleRepo

    @Autowired
    lateinit var userLogsService: UserLogsService

    @Autowired
    lateinit var notificationService: NotificationService
// returns all tasks from the server
override fun getAllTasks(principal: Principal): List<TaskInfoDTO> {
        userLogsService.addLog(UserLogs(principal.name, "Get all tasks"))

        return taskRepo.findAll().map { TaskInfoDTO().toDTO(it) }
    }
//returns all tasks of a user for current day
    override fun getUsersTasks(userName: String, principal: Principal): List<TaskInfoDTO> {
//gets user data by email
        val me: User = userRepo.findByEmail(userName) ?: throw IdNotFoundException()

        var tasks: MutableList<Task> = mutableListOf()
//    if user has schedule, get the working hours for today

        if (me.schedule != null) {
            val userSchedule = getWorkingTime(me.schedule!!)
            when {
//                if today user's off day, return empty list
                userSchedule.isOffDay -> {
                    tasks = mutableListOf()
                }
//                gets all tasks of a user where start time is within working hours
                userSchedule.workingHours.isNotEmpty() -> {
                    tasks.addAll(taskRepo.findAllByAssignedUsersContainsOrderByTaskStartAsc(me))
                    tasks.addAll(taskRepo.findAllByAssignedRoleIdAndTaskStartBetweenOrderByTaskStartAsc(me.role.id!!, userSchedule.workingHours.get(0), userSchedule.workingHours.get(1)))
                }
                else -> {
                    tasks.addAll(taskRepo.findAllByAssignedUsersContainsOrAssignedRoleIdOrderByTaskStartAsc(me, me.role.id!!))

                }
            }
        } else {
            val midnight: LocalTime = LocalTime.MIDNIGHT
            val today: LocalDate = LocalDate.now()
            val todayM=LocalDateTime.of(today, midnight)
            val todayMidnight: Date =  Date.from(todayM.atZone(ZoneId.systemDefault()).toInstant())
            val tomorrowMidnight: Date = Date.from(todayM.plusDays(1).atZone(ZoneId.systemDefault()).toInstant())

            tasks.addAll(taskRepo.findAllByAssignedUsersContainsOrAssignedRoleIdAndTaskStartBetweenOrderByTaskStartAsc(me, me.role.id!!,todayMidnight, tomorrowMidnight))
        }

        return tasks.distinctBy { it.id }.map { TaskInfoDTO().toDTO(it) }
    }

//create a new task on the server
    override fun createTask(dto: TaskCreateDTO, userName: String): TaskInfoDTO {

        val creator: User = userRepo.findByEmail(userName) ?: throw IdNotFoundException()
        val assignedUsers = if (dto.assignedUserIds != null) {
            val result = userRepo.findAllByIdIn(dto.assignedUserIds!!.toMutableList())
            result.toMutableList()
        } else {
            null
        }
        val assignedRole: Role? = if (dto.assignedRoleId != null) {
            roleRepo.findByIdOrNull(dto.assignedRoleId!!) ?: throw IdNotFoundException()
        } else {
            null
        }

        val entity: Task = dto.toEntity(dto, creator, assignedUsers, assignedRole)
        taskRepo.save(entity);
        userLogsService.addLog(UserLogs(userName, "Save task ${entity.toString()}"))
//    send notification to the recipient of a new task
        sendNotification(entity)
        return TaskInfoDTO().toDTO(entity)

    }
//send firebase notification to a user within working hours
    fun sendNotification(task: Task) {
        var tokens = mutableListOf<String>()
        if (task.assignedRole != null) {
            val users = userRepo.findAllByRoleIdAndFirebaseTokenIsNotNull(task.assignedRole?.id!!)
            if (users.isNotEmpty()) {
                users.forEach {
                    val workingHourStatus = checkWorkingHours(it.schedule)
                    if (workingHourStatus == WorkingHourStatus.ISWORKINGHOUR || workingHourStatus == WorkingHourStatus.NODATA) {
                        tokens.add(it.firebaseToken!!)
                    }
                }

            }

        } else if (task.assignedUsers != null) {
            var userIds = task.assignedUsers?.map { it.id!! }?.toMutableList()
            if (userIds != null) {
                val users = userRepo.findAllByIdInAndFirebaseTokenIsNotNull(userIds)
                if (users.isNotEmpty()) {
                    users.forEach {
                        val workingHourStatus = checkWorkingHours(it.schedule)
                        if (workingHourStatus == WorkingHourStatus.ISWORKINGHOUR || workingHourStatus == WorkingHourStatus.NODATA) {
                            tokens.add(it.firebaseToken!!)
                        }
                    }
                }


            }
        }


        if (tokens.isNotEmpty()) {
            notificationService.sendNotificationsToUsers(tokens, task.id!!, NotificationUtils.NOTIF_CODE_NEW_TASK)

        }

    }
//update task progress status on the server
    override fun updateTaskStatus(dto: TaskStatusUpdateDTO, principal: Principal): TaskInfoDTO {
        var task: Task = taskRepo.findByIdOrNull(dto.taskId) ?: throw  IdNotFoundException()

        val user: User = userRepo.findByEmail(principal.name) ?: throw  IdNotFoundException()

        task.status = TaskStatus.fromCode(dto.status)
        task.modifiedDate = Date()
        task.modifiedBy = user
        taskRepo.save(task)
        return TaskInfoDTO().toDTO(task)
    }
//delete task from the server
    override fun deleteTask(id: Int, principal: Principal) {
        userLogsService.addLog(UserLogs(principal.name, "Delete task with id=$id"))
        taskRepo.deleteById(id)
    }
//return list of tasks with a give constraints
    fun filterTasks(dto: TaskFilterDTO, principal: Principal): List<TaskInfoDTO> {
        val me: User = userRepo.findByEmail(principal.name) ?: throw IdNotFoundException()
        val assignedUsers: MutableList<User> = mutableListOf()
        val createdUsers: MutableList<User> = mutableListOf()
        if (dto.myTasks) {
            createdUsers.add(me)
        } else {
            assignedUsers.add(me)
        }
        if (dto.assignedUsers != null) {
            assignedUsers.addAll(userRepo.findAllById(dto.assignedUsers!!.toMutableList()))
        }

        if (dto.createdUsers != null) {
            createdUsers.addAll(userRepo.findAllById(dto.createdUsers!!.toMutableList()))
        }
        val fromDate = if (dto.fromDate != null) Date(dto.fromDate!!) else Date(0)

        val toDate = if (dto.toDate != null) Date(dto.toDate!!) else Date()
        var result: List<Task> = listOf()
        if (dto.myTasks) {
            result =
                    taskRepo.findAllByAssignedDateIsBetweenAndCreator(fromDate, toDate, me)

        } else {
            when (assignedUsers.size) {
                1 -> {
                    result = if (createdUsers.isNotEmpty()) {
                        taskRepo.findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContains(fromDate, toDate, createdUsers, assignedUsers[0])
                    } else {
                        taskRepo.findAllByAssignedDateIsBetweenAndAssignedUsersContains(fromDate, toDate, assignedUsers[0])
                    }
                }
                2 -> {
                    result = if (createdUsers.isNotEmpty()) {
                        taskRepo.findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, createdUsers, assignedUsers[0], assignedUsers[1])
                    } else {
                        taskRepo.findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, assignedUsers[0], assignedUsers[1])
                    }
                }
                3 -> {
                    result = if (createdUsers.isNotEmpty()) {
                        taskRepo.findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, createdUsers, assignedUsers[0], assignedUsers[1], assignedUsers[2])
                    } else {
                        taskRepo.findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, assignedUsers[0], assignedUsers[1], assignedUsers[2])
                    }
                }
                4 -> {
                    result = if (createdUsers.isNotEmpty()) {
                        taskRepo.findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, createdUsers, assignedUsers[0], assignedUsers[1], assignedUsers[2], assignedUsers[3])
                    } else {
                        taskRepo.findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, assignedUsers[0], assignedUsers[1], assignedUsers[2], assignedUsers[3])
                    }
                }
                5 -> {
                    result = if (createdUsers.isNotEmpty()) {
                        taskRepo.findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, createdUsers, assignedUsers[0], assignedUsers[1], assignedUsers[2], assignedUsers[3], assignedUsers[4])
                    } else {
                        taskRepo.findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate, toDate, assignedUsers[0], assignedUsers[1], assignedUsers[2], assignedUsers[3], assignedUsers[4])
                    }
                }
            }


        }

        return result.map { TaskInfoDTO().toDTO(it) }
    }

}