package dev.slashdash.project.service.task

import dev.slashdash.project.dto.TaskCreateDTO
import dev.slashdash.project.dto.TaskInfoDTO
import dev.slashdash.project.dto.TaskStatusUpdateDTO
import dev.slashdash.project.model.Task
import java.security.Principal

interface ITaskService {
    fun getAllTasks(principal: Principal):List<TaskInfoDTO>
    fun createTask(dto:TaskCreateDTO, userName: String): TaskInfoDTO
    fun updateTaskStatus(dto: TaskStatusUpdateDTO,principal: Principal):TaskInfoDTO
    fun deleteTask(id:Int,principal: Principal)
    fun getUsersTasks(userName: String,principal: Principal):List<TaskInfoDTO>
}