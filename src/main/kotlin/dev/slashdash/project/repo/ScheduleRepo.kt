package dev.slashdash.project.repo


import dev.slashdash.project.model.Schedule
import dev.slashdash.project.model.WorkingHour
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for Schedule java model with custom queries

interface ScheduleRepo : JpaRepository<Schedule, Int> {
    override fun findAll(): List<Schedule>

}