package dev.slashdash.project.repo

import dev.slashdash.project.model.UserLogs
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserLogsRepo:JpaRepository<UserLogs, Int>{
    fun getAllByLogDateIsAfterOrderByLogDateDesc(date:Date,pageable: Pageable): Page<UserLogs>
}