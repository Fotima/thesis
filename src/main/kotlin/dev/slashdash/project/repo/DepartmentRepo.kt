package dev.slashdash.project.repo

import dev.slashdash.project.model.Department
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for Department java model

interface DepartmentRepo : JpaRepository<Department, Int>{

}