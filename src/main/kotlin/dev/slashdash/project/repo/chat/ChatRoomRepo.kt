package dev.slashdash.project.repo.chat

import dev.slashdash.project.model.chat.ChatRoom
import dev.slashdash.project.model.chat.InstantMessage
import dev.slashdash.project.model.security.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
//Jpa repository for chat room java model

@Repository
interface ChatRoomRepo : JpaRepository<ChatRoom, Int> {
    fun findByConnectedUsersContainsAndConnectedUsersContains(user1:User, user2: User):ChatRoom?
    fun findByConnectedUsersContainsOrderByLastMessageTimeDesc(user: User): List<ChatRoom>
}