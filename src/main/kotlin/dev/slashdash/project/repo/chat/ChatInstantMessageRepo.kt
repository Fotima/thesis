package dev.slashdash.project.repo.chat

import dev.slashdash.project.model.chat.InstantMessage
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
//Jpa repository for Instant message java model
@Repository
interface ChatInstantMessageRepo : JpaRepository<InstantMessage, Int>{
        fun  findAllByChatRoom_IdOrderByCreatedBy(chatId:Int, pageable: Pageable): Page<InstantMessage>
}