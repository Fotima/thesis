package dev.slashdash.project.repo

import dev.slashdash.project.model.security.Permission
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for Permission java model with custom queries

interface PermissionRepo: JpaRepository<Permission,Int> {
    fun findByName(name: String): Permission?
    override fun findAll(): List<Permission>
    override fun delete(permission: Permission)
}