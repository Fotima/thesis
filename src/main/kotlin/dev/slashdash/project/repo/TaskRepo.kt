package dev.slashdash.project.repo

import dev.slashdash.project.model.Task
import dev.slashdash.project.model.security.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*
//Jpa repository for Task java model with custom queries

interface TaskRepo : JpaRepository<Task, Int> {
    fun findAllByAssignedUsersContainsOrderByTaskStartAsc(user: User): List<Task>
    fun findAllByAssignedRoleIdAndTaskStartBetweenOrderByTaskStartAsc(roleId: Int, workStart: Date, workEnd: Date): List<Task>
    fun findAllByAssignedUsersContainsOrAssignedRoleIdOrderByTaskStartAsc(user: User, roleId: Int): List<Task>
    fun findAllByAssignedUsersContainsOrAssignedRoleIdAndTaskStartBetweenOrderByTaskStartAsc(user: User, roleId: Int,workStart: Date, workEnd: Date): List<Task>
    fun findAllByAssignedDateIsBetweenAndCreator(fromDate: Date?, endDate: Date?, user: User): List<Task>
    fun  findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContains(fromDate: Date?, endDate: Date?, cretorUsers: List<User>?, user:User):List<Task>
    fun  findAllByAssignedDateIsBetweenAndAssignedUsersContains(fromDate: Date?, endDate: Date?,  user:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?, endDate: Date?,  user1:User, user2:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?,endDate: Date?, creatorUsers:List<User>? , user1:User, user2:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?,endDate: Date?, creatorUsers:List<User>? , user1:User, user2:User, user3:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?, endDate: Date?,  user1:User, user2:User, user3:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?,endDate: Date?, creatorUsers:List<User>? , user1:User, user2:User, user3:User,user4:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?, endDate: Date?,  user1:User, user2:User, user3:User, user4:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndCreatorInAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?,endDate: Date?, creatorUsers:List<User>? , user1:User, user2:User, user3:User,user4:User,user5:User):List<Task>
    fun findAllByAssignedDateIsBetweenAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContainsAndAssignedUsersContains(fromDate: Date?, endDate: Date?,  user1:User, user2:User, user3:User, user4:User,user5:User):List<Task>

}