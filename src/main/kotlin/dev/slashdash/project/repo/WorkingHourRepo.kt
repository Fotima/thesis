package dev.slashdash.project.repo

import dev.slashdash.project.model.WorkingHour
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for WorkingHour java model with custom queries

interface WorkingHourRepo : JpaRepository<WorkingHour, Int> {
    override fun findAll(): List<WorkingHour>

}