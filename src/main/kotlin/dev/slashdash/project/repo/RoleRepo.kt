package dev.slashdash.project.repo

import dev.slashdash.project.model.security.Role
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for Role java model with custom queries

interface RoleRepo : JpaRepository<Role, Int> {
    fun findByName(name: String): Role?
    override fun findAll(): List<Role>
    fun findAllByRankGreaterThan(rank:Int):List<Role>
    fun countAllByRankGreaterThan(rank:Int):Int
}
