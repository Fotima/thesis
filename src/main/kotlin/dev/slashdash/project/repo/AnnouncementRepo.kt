package dev.slashdash.project.repo

import dev.slashdash.project.model.Announcement
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for Announcement java model

interface AnnouncementRepo :JpaRepository<Announcement, Int> {
    fun findAllByOrderByDateDesc():List<Announcement>
}