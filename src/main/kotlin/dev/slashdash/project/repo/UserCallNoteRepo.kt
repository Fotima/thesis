package dev.slashdash.project.repo

import dev.slashdash.project.model.UserCallNote
import org.springframework.data.jpa.repository.JpaRepository
//Jpa repository for UserCallNote java model with custom queries

interface UserCallNoteRepo : JpaRepository<UserCallNote, Int>{
        fun getAllByCallingUserEmail(email:String):List<UserCallNote>
}