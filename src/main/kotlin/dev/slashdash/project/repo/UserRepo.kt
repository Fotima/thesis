package dev.slashdash.project.repo

import dev.slashdash.project.dto.UserInfoDTO
import dev.slashdash.project.model.security.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*
//Jpa repository for User java model with custom queries

interface UserRepo : JpaRepository<User, Int> {


    fun findAllByActiveTrue(): List<User>
    fun findByEmail(email: String): User?

    fun findByEmployeeId(employeeId: String): Optional<User>
    override fun deleteById(id: Int)

    fun findAllByRoleRankIsGreaterThan(rank: Int): List<User>
    fun findTop10ByCheckedInTrue():List<User>
    fun countAllByActiveTrue():Int
    fun countAllByCheckedInTrue():Int
    fun findAllByIdInAndFirebaseTokenIsNotNull(ids:MutableList<Int>):List<User>
    fun findAllByRoleIdAndFirebaseTokenIsNotNull(roleId:Int):List<User>
    fun findAllByEmailNotAndRoleNameNotOrderByFirstname(email:String, adminRole:String):List<User>
    fun findAllByIdAndRoleRankIsGreaterThanOrRoleRankAndRoleNameNotOrderByFirstname(id:Int?, rank:Int?,rank1:Int?, adminRole:String):List<User>
    fun findAllByIdAndRoleRankIsGreaterThanAndRoleNameNotOrderByFirstname(id:Int?, rank:Int?, adminRole:String):List<User>
    fun findAllByIdIn(ids:MutableList<Int>):List<User>

}
