package dev.slashdash.project.exception
//Custom exceptions used on the application
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
var logger: Logger = LogManager.getLogger("LoggedExceptions")

fun LoggedError(ex: RuntimeException): RuntimeException {
    println("EXCEPTION LOG: ${ex.javaClass.simpleName} with message: ${ex.message}")
    logger.error("EXCEPTION LOG: ${ex.javaClass.simpleName} with message: ${ex.message}")
    return ex
}
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class BadRequestException : RuntimeException()

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class InvalidJWTTokenException : Exception("Expired or invalid JWT token!")

@ResponseStatus(code = HttpStatus.CONFLICT)
class EmailNotAvailableException : Exception("Email is not available!")

@ResponseStatus(code = HttpStatus.CONFLICT)
class UsernameNotAvailableException : Exception("Username is not available!")

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class EmptyFileException : Exception("File is empty!")

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class EmptyIdException : Exception("Id is required!")

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
class FileNotUploadedException : Exception("File not uploaded!")

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
class InternalServerErrorException : RuntimeException()

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
class InvalidCredentialsException : Exception("Username or password is incorrect!")

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class InvalidTypeException : Exception("Input type is incorrect!")

@ResponseStatus(code = HttpStatus.NOT_FOUND)
class NotFoundException : RuntimeException()

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class NullFileException : Exception("File is null!")

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class PasswordValidationException(message : String) : Exception(message)

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
class UserNotCreatedException(error: String) : java.lang.RuntimeException(error)

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class UserNotFoundException() : Exception("User not found!")

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class GoogleUserNotFoundException() : Exception("User with this google account is not found!")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class EntryNotFoundException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class IdNotFoundException : RuntimeException("Id not found!")

@ResponseStatus(HttpStatus.CONFLICT)
class EntityNotCreatedException(exception: String) : RuntimeException(exception)


@ResponseStatus(HttpStatus.BAD_REQUEST)
class CustomForbiddenException(msg: String) : RuntimeException(msg)

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class CustomBadRequestException(msg: String) : RuntimeException(msg)